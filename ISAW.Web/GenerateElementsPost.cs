﻿using ISAW.Core;

namespace ISAW.Web
{
    public class GenerateElementsPost<TParent>
    {
        public TParent Parent { get; set; }
        public CombatCommand CombatCommand { get; set; }
    }
}
