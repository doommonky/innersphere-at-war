using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Cors;
using ISAW.Core;
using ISAW.DAL;

namespace ISAW.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //TODO: Remove workaround when deployed.
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder => { builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                    });
            });

            services.AddSingleton<ICombatCommandCompositions, CombatCommandCompositions>();
            services.AddTransient<IRandomLanceCompositions, RandomLanceCompositions>();
            services.AddTransient<IRandomBattalionsCompositionsGenerator, RandomBattalionCompositionsGenerator>();
            services.AddTransient<IRATGenerator, RATGenerator>();

            //Generator Services
            services.AddSingleton<ICombatCommandCompositionGenerator, CombatCommandCompositionGenerator>();
            services.AddSingleton<ICombatValuesGenerator, CombatValuesGenerator>();
            services.AddSingleton<ICompositionGenerator, CompositionGenerator>();

            services.AddTransient<IElementGenerator<CombatCommand, Regiment>, RegimentGenerator>();
            services.AddTransient<IElementGenerator<Regiment, Battalion>, BattalionGenerator>();
            services.AddTransient<IElementGenerator<Battalion, Company>, CompanyGenerator>();
            services.AddTransient<IElementGenerator<Company, Lance>, LanceGenerator>();
            services.AddTransient<IElementGenerator<Lance, Element>, ElementGenerator>();

            //DbContext
            services.AddScoped<IDbContext, DbContext>();

            //Repositories
            services.AddTransient<IRepository<CombatCommand>, CombatCommandRepository>();
            services.AddTransient<IRepository<CombatValues>, CombatValuesRepository>();
            services.AddTransient<IRepository<Element>, ElementRepository>();
            services.AddTransient<IRepository<Lance>, LanceRepository>();
            services.AddTransient<IRepository<Company>, CompanyRepository>();
            services.AddTransient<IRepository<Battalion>, BattalionRepository>();
            services.AddTransient<IRepository<Regiment>, RegimentRepository>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseCors("AllowAll");
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
