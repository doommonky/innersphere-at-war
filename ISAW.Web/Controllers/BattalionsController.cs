﻿using Microsoft.AspNetCore.Mvc;
using ISAW.Core;
using ISAW.DAL;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISAW.Web.Controllers
{
    [ApiController]
    [Route("battalions")]
    public class BattalionsController : ControllerBase
    {
        private readonly IDbContext _context;
        private readonly IRepository<Battalion> _battalionRepository;
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<CombatValues> _combatValuesRepository;
        private readonly IElementGenerator<Battalion, Company> _companies;

        public BattalionsController(
            IDbContext context,
            IRepository<Battalion> battalionRepository,
            IRepository<Company> companyRepository,
            IRepository<CombatValues> combatValuesRespository,
            IElementGenerator<Battalion, Company> companies)
        {
            _context = context;
            _battalionRepository = battalionRepository;
            _companyRepository = companyRepository;
            _combatValuesRepository = combatValuesRespository;
            _companies = companies;
        }

        //battalions/{id}
        [HttpGet("{id}")]
        public async Task<Battalion> Get(Guid id)
        {
            using var uow = _context.Create();
            var battalion = await _battalionRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return battalion;
        }

        //battalion/parent/{parentId}
        [HttpGet("parent/{parentId}")]
        public async Task<IEnumerable<Battalion>> GetAll(Guid parentId)
        {
            using var uow = _context.Create();
            var battalions = await _battalionRepository.GetByParentIdAsync(parentId).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return battalions;
        }

        //battalion
        [HttpPost]
        public async Task<Battalion> Post(Battalion battalion)
        {
            using var uow = _context.Create();
            var id = await _battalionRepository.InsertAsync(battalion).ConfigureAwait(false);
            var result = await _battalionRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return result; 
        }


        //battalions/companies
        [HttpPost("companies")]
        public async Task<IEnumerable<Company>> GenerateCompanies(GenerateElementsPost<Battalion> post)
        {
            var companies = _companies.Generate(post.Parent, post.CombatCommand);

            using var uow = _context.Create();
            try
            {
                var companyIds = await _companyRepository.InsertAsync(companies).ConfigureAwait(false);
                var extantCombatValues = companies.Where(r => r.CombatValues != null);
                foreach (var company in extantCombatValues)
                {
                    await _combatValuesRepository.InsertAsync(company.CombatValues).ConfigureAwait(false);
                }
                await uow.CommitAsync().ConfigureAwait(false);
                return companies;
            }
            catch (Exception ex)
            {
                await uow.RollBackAsync().ConfigureAwait(false);
                return null;
            }
        }

        //battalion
        [HttpPut]
        public async Task<Battalion> Put(Battalion battalion)
        {
            using var uow = _context.Create();
            await _battalionRepository.UpdateAsync(battalion).ConfigureAwait(false);
            var result = await _battalionRepository.GetAsync(battalion.Id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return result; 
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            using var uow = _context.Create();
            await _battalionRepository.DeleteAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return Ok();
        }
    }
}
