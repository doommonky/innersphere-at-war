﻿using Microsoft.AspNetCore.Mvc;
using ISAW.Core;
using ISAW.DAL;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISAW.Web.Controllers
{
    [ApiController]
    [Route("combatcommands")]
    public class CombatCommandsController : ControllerBase
    {
        private readonly IElementGenerator<CombatCommand, Regiment> _regimentGenerator;
        private readonly IDbContext _context;
        private readonly IRepository<CombatCommand> _combatCommandRepository;
        private readonly IRepository<Regiment> _regimentRepository;
        private readonly IRepository<CombatValues> _combatValuesRepository;

        private readonly IRepository<Battalion> _battalionRepository;
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<Lance> _lanceRepository;
        private readonly IRepository<Element> _elementRepository;

        public CombatCommandsController(
            IElementGenerator<CombatCommand, Regiment> regimentGenerator,
            IDbContext context,
            IRepository<CombatCommand> combatCommandRepository,
            IRepository<Regiment> regimentRepository,
            IRepository<CombatValues> combatValuesRepository,

            IRepository<Battalion> battalionRepository,
            IRepository<Company> companyRepository,
            IRepository<Lance> lanceRepository,
            IRepository<Element> elementRepository
            )
        {
            _regimentGenerator = regimentGenerator;
            _context = context;
            _combatCommandRepository = combatCommandRepository;
            _regimentRepository = regimentRepository;
            _combatValuesRepository = combatValuesRepository;

            _battalionRepository = battalionRepository;
            _companyRepository = companyRepository;
            _lanceRepository = lanceRepository;
            _elementRepository = elementRepository;

        }

        //combatcommand/{id}
        [HttpGet("{id}")]
        public async Task<CombatCommand> Get(Guid id)
        {
            using var uow = _context.Create();
            var combatCommand = await _combatCommandRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return combatCommand; 
        }

        [HttpGet("{id}/structure/")]
        public async Task<CombatCommand> GetStructure(Guid id)
        {
            using var uow = _context.Create();
            var combatCommand = await _combatCommandRepository.GetAsync(id).ConfigureAwait(false);
            combatCommand.Regiments = (await _regimentRepository.GetByParentIdAsync(combatCommand.Id).ConfigureAwait(false)).OrderBy(r => r.Type).ToArray();
            foreach (var regiment in combatCommand.Regiments)
            {
                regiment.CombatValues = (await _combatValuesRepository.GetByParentIdAsync(regiment.Id).ConfigureAwait(false)).FirstOrDefault();

                regiment.Battalions = (await _battalionRepository.GetByParentIdAsync(regiment.Id).ConfigureAwait(false)).ToArray();
                foreach (var battalion in regiment.Battalions)
                {
                    battalion.CombatValues = (await _combatValuesRepository.GetByParentIdAsync(battalion.Id).ConfigureAwait(false)).FirstOrDefault();

                    battalion.Companies = (await _companyRepository.GetByParentIdAsync(battalion.Id).ConfigureAwait(false)).ToArray();
                    foreach (var company in battalion.Companies)
                    {
                        company.CombatValues = (await _combatValuesRepository.GetByParentIdAsync(company.Id).ConfigureAwait(false)).FirstOrDefault();
                        company.Lances = (await _lanceRepository.GetByParentIdAsync(company.Id).ConfigureAwait(false)).ToArray();

                        foreach (var lance in company.Lances)
                        {
                            lance.CombatValues = (await _combatValuesRepository.GetByParentIdAsync(lance.Id).ConfigureAwait(false)).FirstOrDefault();
                            lance.Elements = (await _elementRepository.GetByParentIdAsync(lance.Id).ConfigureAwait(false)).ToArray();
                        }
                    }
                }
            }

            await uow.CommitAsync().ConfigureAwait(false);
            return combatCommand;
        }

        //combatcommand?allegence={factionName}
        [HttpGet]
        public async Task<IEnumerable<CombatCommand>> GetAll([FromQuery]Faction allegiance)
        {
            using var uow = _context.Create();
            var combatCommands = await _combatCommandRepository.GetByFactionAsync(allegiance).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            uow.Dispose();
            return combatCommands;
        }

        //combatcommand
        [HttpPost]
        public async Task<CombatCommand> Post(CombatCommand combatCommand)
        {
            using var uow = _context.Create();
            var id = await _combatCommandRepository.InsertAsync(combatCommand).ConfigureAwait(false);
            await uow.CommitAsync();
            return await _combatCommandRepository.GetAsync(id).ConfigureAwait(false);
        }

        [HttpPost("regiments")]
        public async Task<IEnumerable<Regiment>> GenerateRegiments([FromBody]CombatCommand combatCommand)
        {
            var regiments = _regimentGenerator.Generate(combatCommand);

            using var uow = _context.Create();
            try
            {
                await _regimentRepository.InsertAsync(regiments).ConfigureAwait(false);
                var extantCombatValues = regiments.Where(r => r.CombatValues != null);
                foreach (var regiment in extantCombatValues)
                {
                    await _combatValuesRepository.InsertAsync(regiment.CombatValues).ConfigureAwait(false);
                }
                await uow.CommitAsync().ConfigureAwait(false);
                return regiments;
            }
            catch (Exception ex)
            {
                await uow.RollBackAsync().ConfigureAwait(false);
                return null;
            }
        }

        //combatcommand
        [HttpPut]
        public async Task<CombatCommand> Put(CombatCommand combatCommand)
        {
            using var uow = _context.Create();
            await _combatCommandRepository.UpdateAsync(combatCommand).ConfigureAwait(false);
            var result = await _combatCommandRepository.GetAsync(combatCommand.Id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);

            return result;
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            using var uow = _context.Create();
            await _combatCommandRepository.DeleteAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return Ok();
        }
    }
}
