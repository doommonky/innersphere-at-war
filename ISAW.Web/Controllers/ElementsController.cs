﻿using Microsoft.AspNetCore.Mvc;
using ISAW.Core;
using ISAW.DAL;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace ISAW.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ElementsController : ControllerBase
    {
        private readonly IDbContext _dbContext;
        private readonly IRepository<Element> _elementRepository;
        public ElementsController(IDbContext dbContext, IRepository<Element> elementRepository)
        {
            _dbContext = dbContext;
            _elementRepository = elementRepository;
        }

        //element/{id}
        [HttpGet("{id}")]
        public async Task<Element> Get(Guid id)
        {
            var element = await _elementRepository.GetAsync(id).ConfigureAwait(false);
            return element;
        }

        //element/parent/{parentId}
        [HttpGet("parent/{parentId}")]
        public async Task<IEnumerable<Element>> GetAll(Guid parentId)
        {
            using var uow = _dbContext.Create();
            var elements = await _elementRepository.GetByParentIdAsync(parentId).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            uow.Dispose();
            return elements;
        }

        //element
        [HttpPost]
        public async Task<Element> Post(Element element)
        {
            var id = await _elementRepository.InsertAsync(element).ConfigureAwait(false);
            return await _elementRepository.GetAsync(id).ConfigureAwait(false);
        }

        //element
        [HttpPut]
        public async Task<Element> Put(Element element)
        {
            await _elementRepository.UpdateAsync(element).ConfigureAwait(false);
            return await _elementRepository.GetAsync(element.Id).ConfigureAwait(false);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _elementRepository.DeleteAsync(id).ConfigureAwait(false);
            return Ok();
        }
    }
}
