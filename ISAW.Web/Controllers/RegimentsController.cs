﻿using Microsoft.AspNetCore.Mvc;
using ISAW.Core;
using ISAW.DAL;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISAW.Web.Controllers
{
    [ApiController]
    [Route("regiments")]
    public class RegimentsController : ControllerBase
    {
        private readonly IRepository<Regiment> _regimentRepository;
        private readonly IRepository<Battalion> _battalionRepository;
        private readonly IRepository<CombatValues> _combatValuesRepository;
        private readonly IElementGenerator<Regiment, Battalion> _battalionGenerator;
        private readonly IDbContext _context;
        public RegimentsController(IRepository<Regiment> regimentRepository, IRepository<Battalion> battalionRepository,
            IRepository<CombatValues> combatValuesRepository, IElementGenerator<Regiment, Battalion> battalionGenerator, IDbContext context)
        {
            _regimentRepository = regimentRepository;
            _battalionRepository = battalionRepository;
            _combatValuesRepository = combatValuesRepository;
            _battalionGenerator = battalionGenerator;
            _context = context;
        }

        //regiment/{id}
        [HttpGet("{id}")]
        public async Task<Regiment> Get(Guid id)
        {
            using var uow = _context.Create();
            var regiment = await _regimentRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return regiment;
        }

        //regiment/parent/{parentId}
        [HttpGet("parent/{parentId}")]
        public async Task<IEnumerable<Regiment>> GetAll(Guid parentId)
        {
            using var uow = _context.Create();
            var regiments = await _regimentRepository.GetByParentIdAsync(parentId).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            uow.Dispose();
            return regiments;
        }

        //Regiment
        [HttpPost]
        public async Task<Regiment> Post(Regiment regiment)
        {
            using var uow = _context.Create();
            var id = await _regimentRepository.InsertAsync(regiment).ConfigureAwait(false);
            var regiments = await _regimentRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return regiments;
        }

        [HttpPost("battalions")]
        public async Task<IEnumerable<Battalion>> GenerateBattalions(GenerateElementsPost<Regiment> post)
        {
            var battalions = _battalionGenerator.Generate(post.Parent, post.CombatCommand);

            using var uow = _context.Create();
            try
            {
                var battalionIds = await _battalionRepository.InsertAsync(battalions).ConfigureAwait(false);
                var extantCombatValues = battalions.Where(r => r.CombatValues != null);
                foreach (var battalion in extantCombatValues)
                {
                    await _combatValuesRepository.InsertAsync(battalion.CombatValues).ConfigureAwait(false);
                }
                await uow.CommitAsync().ConfigureAwait(false);
                return battalions;
            }
            catch (Exception ex)
            {
                await uow.RollBackAsync().ConfigureAwait(false);
                return null;
            }
        }

        //Regiment
        [HttpPut]
        public async Task<Regiment> Put(Regiment regiment)
        {
            using var uow = _context.Create();
            await _regimentRepository.UpdateAsync(regiment).ConfigureAwait(false);
            var result = await _regimentRepository.GetAsync(regiment.Id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return result;
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            using var uow = _context.Create();
            await _regimentRepository.DeleteAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return Ok();
        }
    }
}
