﻿using Microsoft.AspNetCore.Mvc;
using ISAW.Core;
using ISAW.DAL;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISAW.Web.Controllers
{
    [ApiController]
    [Route("companies")]
    public class CompaniesController : ControllerBase
    {
        private readonly IDbContext _context;
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<Lance> _lanceRepository;
        private readonly IRepository<CombatValues> _combatValuesRepository;
        private readonly IElementGenerator<Company, Lance> _lances;
        public CompaniesController(IRepository<Company> companyRepository,
            IRepository<Lance> lanceRepository,
            IRepository<CombatValues> combatValuesRepository,
            IElementGenerator<Company, Lance> lances,
            IDbContext context)
        {
            _context = context;
            _companyRepository = companyRepository;
            _lanceRepository = lanceRepository;
            _combatValuesRepository = combatValuesRepository;
            _lances = lances;
        }

        //company/{id}
        [HttpGet("{id}")]
        public async Task<Company> Get(Guid id)
        {
            using var uow = _context.Create();
            var company = await _companyRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return company;
        }

        //companies/parent/{parentId}
        [HttpGet("parent/{parentId}")]
        public async Task<IEnumerable<Company>> GetAll(Guid parentId)
        {
            using var uow = _context.Create();
            var companies = await _companyRepository.GetByParentIdAsync(parentId).ConfigureAwait(false);
            await uow.CommitAsync();
            return companies;
        }

        [HttpPost]
        public async Task<Company> Post(Company company)
        {
            using var uow = _context.Create();
            var id = await _companyRepository.InsertAsync(company).ConfigureAwait(false);
            var result = await _companyRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return result;
        }


        //companies/lances
        [HttpPost("lances")]
        public async Task<IEnumerable<Lance>> GenerateLances(GenerateElementsPost<Company> post)
        {
            var lances = _lances.Generate(post.Parent, post.CombatCommand);

            using var uow = _context.Create();
            try
            {
                var lanceIds = await _lanceRepository.InsertAsync(lances).ConfigureAwait(false);
                var extantCombatValues = lances.Where(r => r.CombatValues != null);
                foreach (var company in extantCombatValues)
                {
                    await _combatValuesRepository.InsertAsync(company.CombatValues).ConfigureAwait(false);
                }
                await uow.CommitAsync().ConfigureAwait(false);
                return lances;
            }
            catch (Exception ex)
            {
                await uow.RollBackAsync().ConfigureAwait(false);
                return null;
            }
        }

        //company
        [HttpPut]
        public async Task<Company> Put(Company company)
        {
            using var uow = _context.Create();
            await _companyRepository.UpdateAsync(company).ConfigureAwait(false);
            var result = await _companyRepository.GetAsync(company.Id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return result;
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            using var uow = _context.Create();
            await _companyRepository.DeleteAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return Ok();
        }
    }
}
