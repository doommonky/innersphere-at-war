﻿using Microsoft.AspNetCore.Mvc;
using ISAW.Core;
using ISAW.DAL;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISAW.Web.Controllers
{
    [ApiController]
    [Route("lances")]
    public class LancesController : ControllerBase
    {
        private readonly IDbContext _context;
        private readonly IRepository<Lance> _lanceRepository;
        private readonly IRepository<CombatValues> _combatValuesRepository;
        private readonly IElementGenerator<Lance, Element> _lances;
        public LancesController(IRepository<Lance> lanceRepository,
            IRepository<CombatValues> combatValuesRepository,
            IElementGenerator<Lance, Element> lances,
            IDbContext context)
        {
            _context = context;
            _lanceRepository = lanceRepository;
            _combatValuesRepository = combatValuesRepository;
            _lances = lances;
        }

        //lance/{id}
        [HttpGet("{id}")]
        public async Task<Lance> Get(Guid id)
        {
            using var uow = _context.Create();
            var lance = await _lanceRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return lance;
        }

        //lance/parent/{parentId}
        [HttpGet("parent/{parentId}")]
        public async Task<IEnumerable<Lance>> GetAll(Guid parentId)
        {
            using var uow = _context.Create();
            var lances = await _lanceRepository.GetByParentIdAsync(parentId).ConfigureAwait(false);
            await uow.CommitAsync();
            return lances;
        }

        [HttpPost]
        public async Task<Lance> Post(Lance lance)
        {
            using var uow = _context.Create();
            var id = await _lanceRepository.InsertAsync(lance).ConfigureAwait(false);
            var result = await _lanceRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return result;
        }

        //lance
        [HttpPut]
        public async Task<Lance> Put(Lance lance)
        {
            using var uow = _context.Create();
            await _lanceRepository.UpdateAsync(lance).ConfigureAwait(false);
            var result = await _lanceRepository.GetAsync(lance.Id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return result;
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            using var uow = _context.Create();
            await _lanceRepository.DeleteAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return Ok();
        }
    }
}
