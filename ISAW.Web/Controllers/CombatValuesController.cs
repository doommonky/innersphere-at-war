﻿using Microsoft.AspNetCore.Mvc;
using ISAW.Core;
using ISAW.DAL;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISAW.Web.Controllers
{
    [ApiController]
    [Route("combatvalues")]
    public class CombatValuesController : ControllerBase
    {
        private readonly IDbContext _context;
        private readonly IRepository<CombatValues> _combatValuesRepository;
        public CombatValuesController(IRepository<CombatValues> combatValuesRepository, IDbContext context)
        {
            _context = context;
            _combatValuesRepository = combatValuesRepository;
        }

        //combatValues/{id}
        [HttpGet("{id}")]
        public async Task<CombatValues> Get(Guid id)
        {
            using var uow = _context.Create();
            var combatValues = await _combatValuesRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return combatValues;
        }

        //combatValues/parent/{parentId}
        [HttpGet("parent/{parentId}")]
        public async Task<CombatValues> GetByParent(Guid parentId)
        {
            using var uow = _context.Create();
            var combatValues = await _combatValuesRepository.GetByParentIdAsync(parentId).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return combatValues?.FirstOrDefault(); 
        }

        //CombatValues
        [HttpPost]
        public async Task<CombatValues> Post(CombatValues CombatValues)
        {
            using var uow = _context.Create();
            var id = await _combatValuesRepository.InsertAsync(CombatValues).ConfigureAwait(false);
            var combatValues = await _combatValuesRepository.GetAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return combatValues; 
        }

        //CombatValues
        [HttpPut]
        public async Task<CombatValues> Put(CombatValues CombatValues)
        {
            using var uow = _context.Create();
            await _combatValuesRepository.UpdateAsync(CombatValues).ConfigureAwait(false);
            var combatvalues = await _combatValuesRepository.GetAsync(CombatValues.Id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return combatvalues;
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            using var uow = _context.Create();
            await _combatValuesRepository.DeleteAsync(id).ConfigureAwait(false);
            await uow.CommitAsync().ConfigureAwait(false);
            return Ok();
        }
    }
}
