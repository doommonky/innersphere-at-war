﻿using Microsoft.AspNetCore.Mvc;
using ISAW.Core;
using ISAW.DAL;
using System.Linq;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Http;
using System.Text;

namespace ISAW.Web.Controllers
{
    [Route("generations")]
    [ApiController]
    public class GenerationsController : ControllerBase
    {
        private readonly IDbContext _context;
        private readonly ICombatCommandCompositionGenerator _combatCommandGenerator;
        private readonly IRepository<CombatValues> _combatValuesRepository;
        private readonly IRepository<CombatCommand> _combatCommandRepository;
        private readonly IRepository<Regiment> _regimentsRepository;
        private readonly IRepository<Battalion> _battalionsRepository;
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<Lance> _lanceRepository;
        private readonly IRepository<Element> _elementRepository;

        public GenerationsController(IDbContext context,
            ICombatCommandCompositionGenerator combatCommandGenerator,
            IRepository<CombatValues> combatValuesRepository,
            IRepository<CombatCommand> combatCommandRepository,
            IRepository<Regiment> regimentsRepository,
            IRepository<Battalion> battalionsRepository,
            IRepository<Company> companyRepository,
            IRepository<Lance> lanceRepository,
            IRepository<Element> elementRepository)
        {
            _context = context;
            _combatCommandGenerator = combatCommandGenerator;
            _combatValuesRepository = combatValuesRepository;
            _combatCommandRepository = combatCommandRepository;
            _combatCommandGenerator = combatCommandGenerator;
            _regimentsRepository = regimentsRepository;
            _battalionsRepository = battalionsRepository;
            _companyRepository = companyRepository;
            _lanceRepository = lanceRepository;
            _elementRepository = elementRepository;
        }

        [HttpPost, Route("upload")]
        public async Task<IActionResult> UploadAndGenerateCombatCommands(IFormFile file)
        {
            using (var stream = file.OpenReadStream())
            {
                var bytes = new byte[stream.Length];
                await stream.ReadAsync(bytes, 0, (int)stream.Length).ConfigureAwait(false);

                var contents = Encoding.ASCII.GetString(bytes);
                var csv = contents.Split(Environment.NewLine);

                var headerRow = csv[0].Split(',');

                foreach(var line in csv.Skip(1))
                {
                    try
                    {
                        var values = line.Split(',');

                        var faction = (Faction)Enum.Parse(typeof(Faction), (values[Array.IndexOf(headerRow, "Owner")]).Replace(" ", ""));
                        var combatCommand = new CombatCommand
                        {
                            Name = values[Array.IndexOf(headerRow, "Name")],
                            Experience = (Experience)Enum.Parse(typeof(Experience), values[Array.IndexOf(headerRow, "Experience")]),
                            Loyalty = (Loyalty)Enum.Parse(typeof(Loyalty), values[Array.IndexOf(headerRow, "Loyalty")]),
                            Weight = (Weight)Enum.Parse(typeof(Weight), values[Array.IndexOf(headerRow, "Weight")]),
                            Strength = int.Parse(values[Array.IndexOf(headerRow, "Strength")]),
                            Location = values[Array.IndexOf(headerRow, "Location")],
                            ExSLDF = bool.Parse(values[Array.IndexOf(headerRow, "Ex-SLDF?")]),
                            Allegiance = faction,
                            ActiveContract = faction 
                        };

                        await Generate(combatCommand).ConfigureAwait(false);
                    }
                    catch (Exception ex) 
                    {
                        return new BadRequestResult();
                    }

                }
            }

            return new OkResult();
        }

        [HttpPost]
        public async Task<CombatCommand> Generate(CombatCommand combatCommand)
        {
            using var uow = _context.Create();
            try
            {
                combatCommand.Id = await _combatCommandRepository.InsertAsync(combatCommand).ConfigureAwait(false);
                _combatCommandGenerator.GenerateCombatCommandComposition(combatCommand);

                foreach (var regiment in combatCommand.Regiments?.ToList())
                {
                    await _regimentsRepository.InsertAsync(regiment).ConfigureAwait(false);
                    await SaveCombatValues(regiment.CombatValues).ConfigureAwait(false);

                    foreach (var battalion in regiment.Battalions?.ToList())
                    {
                        await _battalionsRepository.InsertAsync(battalion).ConfigureAwait(false);
                        await SaveCombatValues(battalion.CombatValues).ConfigureAwait(false);

                        foreach (var company in battalion.Companies?.ToList())
                        {
                            await _companyRepository.InsertAsync(company).ConfigureAwait(false);
                            await SaveCombatValues(company.CombatValues).ConfigureAwait(false);

                            if (company.Lances != null)
                            {
                                foreach (var lance in company.Lances?.ToList())
                                {
                                    await SaveLance(lance).ConfigureAwait(false);
                                    await SaveCombatValues(lance.CombatValues).ConfigureAwait(false);

                                    if (lance.Elements != null)
                                    {
                                        foreach (var element in lance.Elements?.ToList())
                                        {
                                            await SaveElement(element).ConfigureAwait(false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                await uow.CommitAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                await uow.RollBackAsync().ConfigureAwait(false);
                return combatCommand;
            }
            finally
            {
                uow.Dispose();
            }

            return combatCommand;
        }

        private async Task SaveCombatValues(CombatValues combatValues)
        {
            if (combatValues == null)
                return;

            await _combatValuesRepository.InsertAsync(combatValues).ConfigureAwait(false);
        }


        private async Task SaveLance(Lance lance)
        {
            if (lance == null)
                return;

            await _lanceRepository.InsertAsync(lance).ConfigureAwait(false);
        }

        private async Task SaveElement(Element element)
        {
            if (element == null)
                return;

            await _elementRepository.InsertAsync(element).ConfigureAwait(false);
        }
    }
}