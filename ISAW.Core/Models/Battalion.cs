﻿using System;

namespace ISAW.Core
{
    public class Battalion : Element
    {
        public Company[] Companies { get; set; } = Array.Empty<Company>();
    }
}
