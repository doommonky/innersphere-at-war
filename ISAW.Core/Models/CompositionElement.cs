﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISAW.Core
{
    public class CompositionElement
    {
        public CompositionType Type { get; set; }
        public Weight Weight { get; set; }
    }
}
