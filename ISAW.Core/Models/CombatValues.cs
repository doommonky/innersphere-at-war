﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISAW.Core
{
    public class CombatValues
    {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public int Move { get; set; }
        public int? TransportMove { get; set; }
        public int? Jump { get; set; }
        public int TMM { get; set; }
        public int ARM { get; set; }
        public int Short { get; set; }
        public int Medium { get; set; }
        public int Long { get; set; }
        public int? EAero { get; set; }
        public int PointsValue { get; set; }
        public string[] SpecialRules { get; set; }
    }
}
