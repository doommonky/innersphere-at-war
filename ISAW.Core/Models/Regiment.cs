﻿using System;

namespace ISAW.Core
{
    public class Regiment : Element
    {
        public Battalion[] Battalions { get; set; } = Array.Empty<Battalion>();
    }
}
