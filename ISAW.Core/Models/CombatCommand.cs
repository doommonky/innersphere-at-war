﻿namespace ISAW.Core
{
    public class CombatCommand : Element
    {
        public int Strength { get; set; }
        public string Location { get; set; }
        public Regiment[] Regiments { get; set; }
        public bool ExSLDF {get; set;}
    }
}
