﻿using System;

namespace ISAW.Core
{
    public class Element
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public CompositionType? Type { get; set; }
        public string Name { get; set; }
        public Faction Allegiance { get; set; }
        public Faction? ActiveContract { get; set; }
        public Experience Experience { get; set; }
        public Loyalty Loyalty { get; set; }
        public Weight Weight { get; set; }
        public CombatValues CombatValues { get; set;}
    }
}
