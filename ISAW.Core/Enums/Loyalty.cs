﻿namespace ISAW.Core
{
    public enum Loyalty
    {
        Questionable,
        Reliable,
        Fanatical
    }
}
