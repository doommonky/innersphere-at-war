﻿namespace ISAW.Core
{
    public enum Weight
    {
        Light,
        Medium,
        Heavy,
        Assault
    }
}
