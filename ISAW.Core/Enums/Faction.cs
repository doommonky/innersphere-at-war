﻿namespace ISAW.Core
{
    public enum Faction
    {
        //Inner Sphere
        CapellanConfederation,
        DraconisCombine,
        FederatedSuns,
        FreeWorldsLeague,
        LyranCommonwealth,

        //Periphery
        MagistracyOfCanopus,
        OutworldsAlliance,
        TaurianConcordat,

        //Mercinaries
        Mercenaries
    }
}
