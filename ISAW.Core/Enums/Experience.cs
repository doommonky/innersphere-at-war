﻿namespace ISAW.Core
{
    public enum Experience
    {
        Legendary,
        Heroic,
        Elite,
        Veteran,
        Regular,
        Green,
        ReallyGreen,
        WetBehindTheEars
    }
}
