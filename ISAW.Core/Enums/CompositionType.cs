﻿namespace ISAW.Core
{
    public enum CompositionType
    {
        Battlemech,
        Aerospace,
        Armor,
        Infantry,
        InfantryTransport,
        Artillery,
        Dropship
    }
}
