﻿namespace ISAW.Core
{
    public enum ElementType
    {
        CombatCommand,
        Regiment,
        Battalion,
        Company,
        Lance,
        Element
    }
}
