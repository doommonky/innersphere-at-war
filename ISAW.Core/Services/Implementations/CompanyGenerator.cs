﻿using System;
using System.Collections.Generic;

namespace ISAW.Core
{
    public class CompanyGenerator : IElementGenerator<Battalion, Company>
    {
        private readonly ICompositionGenerator _compositions;
        private readonly ICombatValuesGenerator _combatValues;

        public CompanyGenerator(ICompositionGenerator compositions, ICombatValuesGenerator combatValues)
        {
            _compositions = compositions;
            _combatValues = combatValues;
        }

        public IEnumerable<Company> Generate(Battalion parent, CombatCommand combatCommand = null)
        {
            var composition = _compositions.GetCompositionFor(combatCommand.Allegiance, ElementType.Battalion, parent.Type.GetValueOrDefault(), parent.Weight);
            if (composition == null)
                return null;

            var companies = new List<Company>();
            foreach (var element in composition)
            {
                var id = Guid.NewGuid();
                companies.Add(new Company
                {
                    Id = id,
                    ParentId = parent.Id,
                    Type = element.Type,
                    Allegiance = combatCommand.Allegiance,
                    ActiveContract = combatCommand.ActiveContract,
                    Experience = combatCommand.Experience,
                    Loyalty = combatCommand.Loyalty,
                    Weight = element.Weight,
                    CombatValues = _combatValues.GenerateCombatValues(id, ElementType.Company, combatCommand.Allegiance, element.Type, element.Weight),
                }); ;
            };

            return companies;
        }
    }
}
