﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ISAW.Core
{
    public class BattalionGenerator : IElementGenerator<Regiment, Battalion>
    {
        private readonly ICombatValuesGenerator _combatValues;
        private readonly ICompositionGenerator _compositions;
        private readonly IRandomBattalionsCompositionsGenerator _randomCompositions;

        public BattalionGenerator(ICombatValuesGenerator combatValues, ICompositionGenerator compositions,
            IRandomBattalionsCompositionsGenerator randomCompositions)
        {
            _combatValues = combatValues;
            _compositions = compositions;
            _randomCompositions = randomCompositions;
        }

        public IEnumerable<Battalion> Generate(Regiment parent, CombatCommand combatCommand)
        {
            switch (parent.Type)
            {
                case CompositionType.Infantry:
                    return GenerateInfantryBattalions(parent, combatCommand);
                case CompositionType.Aerospace:
                    return GenerateAerospaceWings(parent, combatCommand);
                case CompositionType.Artillery:
                    return GenerateArtilleryBattlions(parent, combatCommand);
                case CompositionType.Armor:
                case CompositionType.Battlemech:
                    return GenerateBattalions(parent, combatCommand);
                default:
                    return null;
            }
        }

        private IEnumerable<Battalion> GenerateInfantryBattalions(Regiment regiment, CombatCommand combatCommand)
        {
            var composition = _compositions.GetCompositionFor(combatCommand.Allegiance, ElementType.Regiment, CompositionType.Infantry, Weight.Light) ?? null;
            return composition?.Select(element =>
            {
                var id = Guid.NewGuid();
                return new Battalion
                {
                    Id = id,
                    ParentId = regiment.Id,
                    Type = element.Type,
                    Allegiance = combatCommand.Allegiance,
                    ActiveContract = combatCommand.ActiveContract,
                    Experience = combatCommand.Experience,
                    Loyalty = combatCommand.Loyalty,
                    Weight = element.Weight,
                    CombatValues = _combatValues.GenerateCombatValues(id, ElementType.Battalion, combatCommand.Allegiance, CompositionType.Infantry, Weight.Light) ?? null,
                };
            }).ToList();
        }

        private IEnumerable<Battalion> GenerateAerospaceWings(Regiment regiment, CombatCommand combatCommand)
        {
            var wings = new List<Battalion>();
                var id = Guid.NewGuid();
                wings.Add(new Battalion
                {
                    Id = id,
                    ParentId = regiment.Id,
                    Type = CompositionType.Aerospace,
                    Allegiance = combatCommand.Allegiance,
                    ActiveContract = combatCommand.ActiveContract,
                    Experience = combatCommand.Experience,
                    Loyalty = combatCommand.Loyalty,
                    Weight = regiment.Weight,
                    CombatValues = _combatValues.GenerateCombatValues(id, ElementType.Battalion, combatCommand.Allegiance, CompositionType.Aerospace, regiment.Weight) ?? null,
                }) ;
            return wings;
        }

        private IEnumerable<Battalion> GenerateArtilleryBattlions(Regiment regiment, CombatCommand combatCommand)
        {
            var id = Guid.NewGuid();
            return new List<Battalion>
                {
                    new Battalion
                    {
                        Id = id,
                        ParentId = regiment.Id,
                        Type = CompositionType.Artillery,
                        Allegiance = combatCommand.Allegiance,
                        ActiveContract = combatCommand.ActiveContract,
                        Experience = combatCommand.Experience,
                        Loyalty = combatCommand.Loyalty,
                        Weight = regiment.Weight,
                        CombatValues = _combatValues.GenerateCombatValues(id, ElementType.Battalion, combatCommand.Allegiance, CompositionType.Artillery, regiment.Weight) ?? null,
                    }
                };
        }

        private IEnumerable<Battalion> GenerateBattalions(Regiment regiment, CombatCommand combatCommand)
        {
            var battalions = _randomCompositions.GetRandomComposition(combatCommand.Weight)
                    .Select(weight =>
                    {
                        var id = Guid.NewGuid();
                        return new Battalion
                        {
                            Id = id,
                            ParentId = regiment.Id,
                            Type = regiment.Type,
                            Allegiance = combatCommand.Allegiance,
                            ActiveContract = combatCommand.ActiveContract,
                            Experience = combatCommand.Experience,
                            Loyalty = combatCommand.Loyalty,
                            Weight = weight,
                            CombatValues = _combatValues.GenerateCombatValues(id, ElementType.Battalion, combatCommand.Allegiance, regiment.Type.Value, weight) ?? null
                        };
                    }).ToList();

            if (regiment.Type == CompositionType.Battlemech && combatCommand.Strength >= 133)
            {
                var id = Guid.NewGuid();
                battalions.Add(new Battalion
                {
                    Id = id,
                    ParentId = regiment.Id,
                    Type = regiment.Type,
                    Allegiance = combatCommand.Allegiance,
                    ActiveContract = combatCommand.ActiveContract,
                    Experience = combatCommand.Experience,
                    Loyalty = combatCommand.Loyalty,
                    Weight = combatCommand.Weight,
                    CombatValues = _combatValues.GenerateCombatValues(id, ElementType.Battalion, combatCommand.Allegiance, regiment.Type.Value, combatCommand.Weight) ?? null,
                });
            }

            return battalions;
        }
    }
}
