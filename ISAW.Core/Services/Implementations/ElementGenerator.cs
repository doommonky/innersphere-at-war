﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISAW.Core
{
    public class ElementGenerator : IElementGenerator<Lance, Element>
    {
        private readonly IRandomLanceCompositions _randomLanceGenerator;
        private readonly IRATGenerator _ratGenerator;

        public ElementGenerator(IRandomLanceCompositions randomLanceGenerator, IRATGenerator ratGenerator)
        {
            _randomLanceGenerator = randomLanceGenerator;
            _ratGenerator = ratGenerator;
        }

        public IEnumerable<Element> Generate(Lance parent, CombatCommand combatCommand = null)
        {
            //TODO: Get RATs for non-Battlemech units.
            switch (parent.Type)
            {
                case CompositionType.Battlemech:
                    return GenerateBattlemechLanceElements(parent, combatCommand);
                case CompositionType.Aerospace:
                case CompositionType.Armor:
                case CompositionType.Infantry:
                case CompositionType.InfantryTransport:
                case CompositionType.Artillery:
                case CompositionType.Dropship:
                default:
                    return Array.Empty<Element>();
            }
        }

        private IEnumerable<Element> GenerateBattlemechLanceElements(Lance parent, CombatCommand combatCommand = null)
        {
            var elements = new List<Element>();
            if (combatCommand.ExSLDF)
            {
                var composition = _randomLanceGenerator.GetRandomComposition(parent.Weight);
                foreach (var weight in composition)
                {
                    var id = Guid.NewGuid();
                    elements.Add(new Element
                    {
                        Id = id,
                        ParentId = parent.Id,
                        Name = _ratGenerator.Roll(weight, combatCommand.Allegiance, combatCommand).Result, 
                        Type = parent.Type,
                        Allegiance = combatCommand.Allegiance,
                        ActiveContract = combatCommand.ActiveContract,
                        Experience = combatCommand.Experience,
                        Loyalty = combatCommand.Loyalty,
                        Weight = weight
                        //TODO: Get AlphaStrike Combat Values for elements.
                        //CombatValues = _combatValues.GenerateCombatValues(id, ElementType.Lance, combatCommand.Allegiance, element.Type, element.Weight),
                    });
                }
                return elements;
            }
            else
            {
                var battlemech = _ratGenerator.Roll(parent.Weight, combatCommand.Allegiance, combatCommand).Result;
                for(var i = 0; i < 4; i++)
                {
                    var id = Guid.NewGuid();
                    elements.Add(new Element
                    {
                        Id = id,
                        ParentId = parent.Id,
                        Name = battlemech,
                        Type = parent.Type,
                        Allegiance = combatCommand.Allegiance,
                        ActiveContract = combatCommand.ActiveContract,
                        Experience = combatCommand.Experience,
                        Loyalty = combatCommand.Loyalty,
                        Weight = parent.Weight
                        //TODO: Get AlphaStrike Combat Values for elements.
                        //CombatValues = _combatValues.GenerateCombatValues(id, ElementType.Lance, combatCommand.Allegiance, element.Type, element.Weight),
                    });
                }

                return elements;
            }
        }
    }
}
