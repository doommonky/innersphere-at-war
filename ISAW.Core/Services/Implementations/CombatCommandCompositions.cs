﻿using System;
using System.Collections.Generic;

namespace ISAW.Core
{
    public class CombatCommandCompositions : ICombatCommandCompositions
    {
        public IDictionary<CompositionType, int> GetCompositionFor(Faction faction)
        {
            CompositionsByFaction.TryGetValue(faction, out var result);
            return result;
        }

        private readonly Dictionary<Faction, Dictionary<CompositionType, int>> CompositionsByFaction = new Dictionary<Faction, Dictionary<CompositionType, int>>
        {
            {
                Faction.CapellanConfederation, new Dictionary<CompositionType, int>
                {
                    {CompositionType.Battlemech, 1},
                    {CompositionType.Aerospace, 2 },
                    {CompositionType.Armor, 3 },
                    {CompositionType.Infantry, 5 },
                    {CompositionType.Artillery, 2 }
                } 
            },
            {
                Faction.DraconisCombine, new Dictionary<CompositionType, int>
                {
                    {CompositionType.Battlemech, 1},
                    {CompositionType.Aerospace, 3 },
                    {CompositionType.Armor, 3 },
                    {CompositionType.Infantry, 5 },
                    {CompositionType.Artillery, 1 }
                }
            },
            {
                Faction.FederatedSuns, new Dictionary<CompositionType, int>
                {
                    {CompositionType.Battlemech, 1},
                    {CompositionType.Aerospace, 2 },
                    {CompositionType.Armor, 3 },
                    {CompositionType.Infantry, 5 },
                    {CompositionType.Artillery, 1 }
                }
            },
            {
                Faction.FreeWorldsLeague, new Dictionary<CompositionType, int>
                {
                    {CompositionType.Battlemech, 1},
                    {CompositionType.Aerospace, 2 },
                    {CompositionType.Armor, 3 },
                    {CompositionType.Infantry, 8 },
                    {CompositionType.Artillery, 1 }
                }
            },
            {
                Faction.LyranCommonwealth, new Dictionary<CompositionType, int>
                {
                    {CompositionType.Battlemech, 1},
                    {CompositionType.Aerospace, 2 },
                    {CompositionType.Armor, 5 },
                    {CompositionType.Infantry, 7 },
                    {CompositionType.Artillery, 1 }
                }
            },
            {
                Faction.MagistracyOfCanopus, new Dictionary<CompositionType, int>
                {
                    {CompositionType.Battlemech, 1},
                    {CompositionType.Aerospace, 2 },
                    {CompositionType.Armor, 3 },
                    {CompositionType.Infantry, 5 },
                    {CompositionType.Artillery, 1 }
                }
            },
            {
                Faction.OutworldsAlliance, new Dictionary<CompositionType, int>
                {
                    {CompositionType.Battlemech, 1},
                    {CompositionType.Aerospace, 10 },
                    {CompositionType.Armor, 4 },
                    {CompositionType.Infantry, 7 },
                    {CompositionType.Artillery, 1 }
                }
            },
            {
                Faction.TaurianConcordat, new Dictionary<CompositionType, int>
                {
                    {CompositionType.Battlemech, 1},
                    {CompositionType.Aerospace, 2 },
                    {CompositionType.Armor, 3 },
                    {CompositionType.Infantry, 5 },
                    {CompositionType.Artillery, 1 }
                }
            },
            {
                Faction.Mercenaries, new Dictionary<CompositionType, int>
                {
                    {CompositionType.Battlemech, 1},
                    {CompositionType.Aerospace, 1 },
                    {CompositionType.Armor, 1 },
                    {CompositionType.Infantry, 1 },
                    {CompositionType.Artillery, 1 }
                }
            }
        };
    }
}
