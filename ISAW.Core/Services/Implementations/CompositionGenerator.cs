﻿using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace ISAW.Core
{
    public class CompositionGenerator : ICompositionGenerator
    {
        private readonly Faction[] _peripheryFactions = {Faction.MagistracyOfCanopus, Faction.OutworldsAlliance,
            Faction.TaurianConcordat, Faction.Mercenaries};

        public IEnumerable<CompositionElement> GetCompositionFor(Faction faction, ElementType elementType, CompositionType compositionType, Weight weight)
        {
            var compositionsByFaction = GetCompositionsByFaction(faction);
            var combatValuesByType = compositionsByFaction?[elementType.ToString()].ToArray();

            var compositionResult = combatValuesByType.FirstOrDefault(comp => (int)comp["Size"] == (int)weight && (int)comp["Type"] == (int)compositionType)?["Composition"];
            if (compositionResult == null)
            {
                return null;
            }

            var composition = compositionResult.Select(element => new CompositionElement
            {
                Type = element["Type"].ToObject<CompositionType>(),
                Weight = element["Size"].ToObject<Weight>()
            });

            return composition;
        }

        private JObject GetCompositionsByFaction(Faction faction)
        {
            //Periphery states share combat values;
            var factionString = faction.ToString();
            if (_peripheryFactions.Contains(faction))
            {
                factionString = "Periphery";
            }

            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"ISAW.Core.Data.Compositions_{factionString}.json";

            var resourceExists = assembly.GetManifestResourceInfo(resourceName) != null;
            if (!resourceExists)
                return null;

            JObject result;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                result = JObject.Parse(reader.ReadToEnd());
            }

            return result;
        }
    }
 }
