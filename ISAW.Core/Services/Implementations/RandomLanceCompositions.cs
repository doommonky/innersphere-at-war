﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISAW.Core
{
    public class RandomLanceCompositions : IRandomLanceCompositions
    {
        //This generator is used when the Combat Command is ExSLDF to generate the lance weight compositions.
        //For Non-ExSLDF Lances, 4 copies of the same mech are used.
        public IEnumerable<Weight> GetRandomComposition(Weight weight)
        {
            var roll = new Random().Next(1, 7);
            return weight switch
            {
                Weight.Light => LightComposition(roll),
                Weight.Medium => MediumComposition(roll),
                Weight.Heavy => HeavyComposition(roll),
                Weight.Assault => AssaultComposition(roll),
                _ => throw new KeyNotFoundException("I don't know how you managed this, but you dun fucked it all up."),
            };
        }

        private IEnumerable<Weight> LightComposition(int roll)
        {
            switch (roll)
            {
                case 1:
                    return new List<Weight> { Weight.Light, Weight.Light, Weight.Light, Weight.Light };
                case 2:
                case 3:
                    return new List<Weight> { Weight.Light, Weight.Light, Weight.Light, Weight.Medium };
                case 4:
                case 5:
                    return new List<Weight> { Weight.Light, Weight.Light, Weight.Medium, Weight.Medium };
                case 6:
                    return new List<Weight> { Weight.Light, Weight.Light, Weight.Medium, Weight.Heavy };
                default:
                    throw new ArgumentOutOfRangeException($"[Light Composition] {roll} is out of range.");
            }
        }

        private IEnumerable<Weight> MediumComposition(int roll)
        {
            switch (roll)
            {
                case 1:
                    return new List<Weight> { Weight.Light, Weight.Medium, Weight.Medium, Weight.Heavy };
                case 2:
                case 3:
                    return new List<Weight> { Weight.Medium, Weight.Medium, Weight.Medium, Weight.Medium };
                case 4:
                case 5:
                    return new List<Weight> { Weight.Medium, Weight.Medium, Weight.Medium, Weight.Heavy };
                case 6:
                    return new List<Weight> { Weight.Medium, Weight.Medium, Weight.Heavy, Weight.Heavy };
                default:
                    throw new ArgumentOutOfRangeException($"[Medium Composition] {roll} is out of range.");
            }
        }

        private IEnumerable<Weight> HeavyComposition(int roll)
        {
            switch (roll)
            {
                case 1:
                    return new List<Weight> { Weight.Medium, Weight.Heavy, Weight.Heavy, Weight.Heavy };
                case 2:
                case 3:
                    return new List<Weight> { Weight.Heavy, Weight.Heavy, Weight.Heavy, Weight.Heavy };
                case 4:
                case 5:
                    return new List<Weight> { Weight.Medium, Weight.Heavy, Weight.Heavy, Weight.Assault };
                case 6:
                    return new List<Weight> { Weight.Heavy, Weight.Heavy, Weight.Heavy, Weight.Assault};
                default:
                    throw new ArgumentOutOfRangeException($"[Heavy Composition] {roll} is out of range.");
            }
        }

        private IEnumerable<Weight> AssaultComposition(int roll)
        {
            switch (roll)
            {
                case 1:
                    return new List<Weight> { Weight.Medium, Weight.Heavy, Weight.Assault, Weight.Assault };
                case 2:
                case 3:
                    return new List<Weight> { Weight.Heavy, Weight.Heavy, Weight.Assault, Weight.Assault };
                case 4:
                case 5:
                    return new List<Weight> { Weight.Heavy, Weight.Assault, Weight.Assault, Weight.Assault };
                case 6:
                    return new List<Weight> { Weight.Assault, Weight.Assault, Weight.Assault, Weight.Assault };
                default:
                    throw new ArgumentOutOfRangeException($"[Heavy Composition] {roll} is out of range.");
            }
        }
    }
}
