﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ISAW.Core
{
    public class RegimentGenerator : IElementGenerator<CombatCommand, Regiment>
    {
        private readonly ICombatCommandCompositions _compositions;
        private readonly ICombatValuesGenerator _combatValues;

        public RegimentGenerator(ICombatCommandCompositions compositions, ICombatValuesGenerator combatValues)
        {
            _compositions = compositions;
            _combatValues = combatValues;
        }

        public IEnumerable<Regiment> Generate(CombatCommand parent, CombatCommand combatCommand = null)
        {
            var factionCompositions = _compositions.GetCompositionFor(parent.Allegiance);

            var regiments = new List<Regiment>();
            foreach (var typeComposition in factionCompositions)
            {
                for (var i = 0; i < typeComposition.Value; i++)
                {
                    var id = Guid.NewGuid();
                    regiments.Add(new Regiment
                    {
                        Id = id,
                        ParentId = parent.Id,
                        Type = typeComposition.Key,
                        Allegiance = parent.Allegiance,
                        ActiveContract = parent.ActiveContract,
                        Experience = parent.Experience,
                        Loyalty = parent.Loyalty,
                        Weight = GetWeight(parent, typeComposition.Key),
                        CombatValues = typeComposition.Key == CompositionType.Infantry ?
                            _combatValues.GenerateCombatValues(id, ElementType.Regiment, parent.Allegiance, CompositionType.Infantry, Weight.Light) ?? null
                            : null
                    }) ;
                }
            }

            return regiments;
        }

        private Weight GetWeight(CombatCommand parent, CompositionType type)
        {
            if (type == CompositionType.Infantry)
                return Weight.Light;

            if (type == CompositionType.Artillery)
                return Weight.Medium;

            if (type == CompositionType.Aerospace)
                return parent.Weight == Weight.Assault ? Weight.Heavy : parent.Weight;

            return parent.Weight;
        }
    }
}
