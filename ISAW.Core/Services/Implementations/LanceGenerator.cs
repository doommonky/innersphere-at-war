﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISAW.Core
{
    public class LanceGenerator : IElementGenerator<Company, Lance>
    {
        private readonly ICompositionGenerator _compositions;
        private readonly ICombatValuesGenerator _combatValues;

        public LanceGenerator(ICompositionGenerator compositions, ICombatValuesGenerator combatValues)
        {
            _compositions = compositions;
            _combatValues = combatValues;
        }

        public IEnumerable<Lance> Generate(Company parent, CombatCommand combatCommand = null)
        {
            var composition = _compositions.GetCompositionFor(combatCommand.Allegiance, ElementType.Company, parent.Type.GetValueOrDefault(), parent.Weight);
            if (composition == null)
                return Array.Empty<Lance>();

            var lances = new List<Lance>();
            foreach (var element in composition)
            {
                var id = Guid.NewGuid();
                lances.Add(new Lance
                {
                    Id = id, 
                    ParentId = parent.Id,
                    Type = element.Type,
                    Allegiance = combatCommand.Allegiance,
                    ActiveContract = combatCommand.ActiveContract,
                    Experience = combatCommand.Experience,
                    Loyalty = combatCommand.Loyalty,
                    Weight = element.Weight,
                    CombatValues = _combatValues.GenerateCombatValues(id, ElementType.Lance, combatCommand.Allegiance, element.Type, element.Weight),
                });
            };

            return lances;
        }
    }
}
