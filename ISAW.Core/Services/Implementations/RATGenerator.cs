﻿using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System;

namespace ISAW.Core
{
    public class RATGenerator : IRATGenerator
    {
        private Dictionary<int, Dictionary<Weight, string>> _factionTable;
        private Dictionary<string, int> _ratModifiers;
        private readonly Faction[] _peripheryFactions = {Faction.MagistracyOfCanopus, Faction.OutworldsAlliance,
            Faction.TaurianConcordat, Faction.Mercenaries};

        public async Task<string> Roll(Weight weight, Faction faction, CombatCommand combatCommand)
        {
            _factionTable = await GetTableByFaction(faction).ConfigureAwait(false);
            var roll = await ApplyRATModifier(new Random().Next(2, 12), combatCommand.Name, faction).ConfigureAwait(false);

            return GetBattlemech(roll, weight);
        }

        public string GetBattlemech(int roll, Weight weight) 
        {
            var result = _factionTable[roll][weight];
            return result;
        }

        public async Task<Dictionary<int, Dictionary<Weight, string>>> GetTableByFaction(Faction faction)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var factionString = _peripheryFactions.Contains(faction) ? "Periphery" : faction.ToString();
            var resourceName = $"ISAW.Core.Data.RAT_{factionString}.csv";

            var resourceExists = assembly.GetManifestResourceInfo(resourceName) != null;
            if (!resourceExists)
                return null;

            var result = new Dictionary<int, Dictionary<Weight, string>>();

            var csv = Array.Empty<string>();
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                csv = (await reader.ReadToEndAsync().ConfigureAwait(false)).Split(Environment.NewLine);
            }

            foreach (var line in csv.Skip(1))
            {
                var splitLine = line.Split(',');
                result.Add(int.Parse(splitLine[0]), new Dictionary<Weight, string>
                {
                    { Weight.Light, splitLine[1] },
                    { Weight.Medium, splitLine[2] },
                    { Weight.Heavy, splitLine[3] },
                    { Weight.Assault, splitLine[4] }
                });
            }

            return result;
        }

        public async Task<Dictionary<string, int>> GetRATModifiersByFaction(Faction faction)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"ISAW.Core.Data.RATModifiers.json";

            var resourceExists = assembly.GetManifestResourceInfo(resourceName) != null;
            if (!resourceExists)
                return null;

            string json = string.Empty;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                json = await reader.ReadToEndAsync().ConfigureAwait(false);
            }
            var jObject = JObject.Parse(json);
            var result = jObject[faction.ToString()]?.ToObject<Dictionary<string, int>>();
            return result;
        }

        public async Task<int> ApplyRATModifier(int roll, string name, Faction faction)
        {
            _ratModifiers = await GetRATModifiersByFaction(faction).ConfigureAwait(false);
            if (_ratModifiers == null)
                return roll;

            foreach (var modifier in _ratModifiers)
            {
                if (name.Equals(modifier.Key) || name.Contains(modifier.Key))
                    return roll + modifier.Value;
            }
            return roll;
        }
    }
 }
