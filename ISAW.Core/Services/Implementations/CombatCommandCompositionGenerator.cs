﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ISAW.Core
{
    public class CombatCommandCompositionGenerator : ICombatCommandCompositionGenerator
    {
        private readonly IElementGenerator<CombatCommand, Regiment> _regiments;
        private readonly IElementGenerator<Regiment, Battalion> _battalions;
        private readonly IElementGenerator<Battalion, Company> _companies;
        private readonly IElementGenerator<Company, Lance> _lances;
        private readonly IElementGenerator<Lance, Element> _elements;

        public CombatCommandCompositionGenerator(
        IElementGenerator<CombatCommand, Regiment> regiments,
        IElementGenerator<Regiment, Battalion> battalions,
        IElementGenerator<Battalion, Company> companies,
        IElementGenerator<Company, Lance> lances,
        IElementGenerator<Lance, Element> elements)
        {
            _regiments = regiments;
            _battalions = battalions;
            _companies = companies;
            _lances = lances;
            _elements = elements;
        }

        public CombatCommand GenerateCombatCommandComposition(CombatCommand combatCommand)
        {
            combatCommand.Regiments = GenerateRegiments(combatCommand);
            foreach (var regiment in combatCommand.Regiments)
            {
                regiment.Battalions = GenerateBattalions(regiment, combatCommand);
                foreach (var battalion in regiment.Battalions)
                {
                    battalion.Companies = GenerateCompanies(battalion, combatCommand);
                    foreach (var company in battalion.Companies)
                    {
                        company.Lances = GenerateLances(company, combatCommand);
                        foreach (var lance in company.Lances)
                        {
                            lance.Elements = GenerateElements(lance, combatCommand);
                        }
                    }
                }
            }

            return combatCommand;
        }

        public Regiment[] GenerateRegiments(CombatCommand combatCommand)
        {
            return _regiments.Generate(combatCommand)?.ToArray();
        }

        public Battalion[] GenerateBattalions(Regiment regiment, CombatCommand combatCommand)
        {
            return _battalions.Generate(regiment, combatCommand)?.ToArray();
        }

        public Company[] GenerateCompanies(Battalion battalion, CombatCommand combatCommand)
        {
            return _companies.Generate(battalion, combatCommand)?.ToArray();
        }

        public Lance[] GenerateLances(Company company, CombatCommand combatCommand)
        {
            return _lances.Generate(company, combatCommand)?.ToArray();
        }

        public Element[] GenerateElements(Lance lance, CombatCommand combatCommand)
        {
            return _elements.Generate(lance, combatCommand)?.ToArray();
        }
    }
}
