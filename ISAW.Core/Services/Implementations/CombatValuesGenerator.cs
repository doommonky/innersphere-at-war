﻿using System.IO;
using System.Reflection;
using System.Linq;
using Newtonsoft.Json.Linq;
using System;

namespace ISAW.Core
{
    public class CombatValuesGenerator : ICombatValuesGenerator 
    {
        private readonly Faction[] _peripheryFactions = {Faction.MagistracyOfCanopus, Faction.OutworldsAlliance, 
            Faction.TaurianConcordat, Faction.Mercenaries};

        public CombatValues GenerateCombatValues(Guid parentId, ElementType elementType, Faction allegiance, CompositionType compostitionType, Weight weight)
        {
            var combatValuesByFaction = GetCombatValuesByFaction(allegiance);
            var combatValuesByType = combatValuesByFaction?[elementType.ToString()].ToArray();

            var combatValues = combatValuesByType?
                .FirstOrDefault(cv => (int)cv["Size"] == (int)weight && (int)cv["Type"] == (int)compostitionType)?["CombatValues"];

            var result = combatValues?.ToObject<CombatValues>();
            if (result == null)
                return result;

            result.Id = Guid.NewGuid();
            result.ParentId = parentId;
            return result;
        }

        private JObject GetCombatValuesByFaction(Faction faction)
        {
            //Periphery states share combat values;
            var factionString = faction.ToString();
            if (_peripheryFactions.Contains(faction))
            {
                factionString = "Periphery";
            }

            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"ISAW.Core.Data.CombatValues_{factionString}.json";

            var resourceExists = assembly.GetManifestResourceInfo(resourceName) != null;
            if (!resourceExists)
                return null;

            JObject result;
            using(Stream stream = assembly.GetManifestResourceStream(resourceName))
            using(StreamReader reader = new StreamReader(stream))
            {
                result = JObject.Parse(reader.ReadToEnd());
            }

            return result;
        }
    }
}
