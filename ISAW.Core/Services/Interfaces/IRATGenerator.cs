﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISAW.Core
{
    public interface IRATGenerator
    {
        Task<string> Roll(Weight weight, Faction faction, CombatCommand combatCommand);
        Task<Dictionary<int, Dictionary<Weight, string>>> GetTableByFaction(Faction faction);
        Task<Dictionary<string, int>> GetRATModifiersByFaction(Faction faction);
        Task<int> ApplyRATModifier(int roll, string name, Faction faction);
    }
}
