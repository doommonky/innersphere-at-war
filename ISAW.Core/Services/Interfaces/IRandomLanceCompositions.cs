﻿using System.Collections.Generic;

namespace ISAW.Core
{
    public interface IRandomLanceCompositions
    {
        IEnumerable<Weight> GetRandomComposition(Weight weight);
    }
}
