﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISAW.Core
{
    public interface IElementGenerator<TParent, TResult>
    {
        IEnumerable<TResult> Generate(TParent parent, CombatCommand combatCommand = null);
    }
}
