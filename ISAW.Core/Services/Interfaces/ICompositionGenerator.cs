﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISAW.Core
{
    public interface ICompositionGenerator
    {
        IEnumerable<CompositionElement> GetCompositionFor(Faction faction, ElementType elementType, CompositionType compositionType, Weight weight);
    }
}
