﻿
using System;

namespace ISAW.Core
{
    public interface ICombatValuesGenerator 
    {
        CombatValues GenerateCombatValues(Guid parentId, ElementType level, Faction allegiance, CompositionType elementType, Weight weight);
    }
}
