﻿namespace ISAW.Core
{
    public interface ICombatCommandCompositionGenerator
    {
        CombatCommand GenerateCombatCommandComposition(CombatCommand combatCommand);
    }
}
