﻿using System.Collections.Generic;

namespace ISAW.Core
{
    public interface IRandomBattalionsCompositionsGenerator
    {
        IEnumerable<Weight> GetRandomComposition(Weight weight);
    }
}
