﻿using System.Collections.Generic;

namespace ISAW.Core
{
    public interface ICombatCommandCompositions
    {
        IDictionary<CompositionType, int> GetCompositionFor(Faction faction);
    }
}
