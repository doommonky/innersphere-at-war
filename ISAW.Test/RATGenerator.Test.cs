﻿using ISAW.Core;
using NUnit.Framework;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace ISAW.Test
{
    [TestFixture]
    public class RATGeneratorTests : TestBase
    {
        private IRATGenerator _generator;
        public RATGeneratorTests()
        {
        }

        [SetUp]
        public void Setup()
        {
            _generator = _serviceProvider.GetService<IRATGenerator>();
        }

        [Test]
        public async Task ReadRAT_CSV()
        {
            var table = await _generator.GetTableByFaction(Faction.FreeWorldsLeague).ConfigureAwait(false);
            Assert.IsNotNull(table);
        }

        [Test]
        public async Task ReadRATModifierJson()
        {
            var table = await _generator.GetRATModifiersByFaction(Faction.FreeWorldsLeague).ConfigureAwait(false);
            Assert.IsNotNull(table);
        }

        [Test]
        public async Task ApplyRatModifierJson()
        {
            var table = await _generator.GetRATModifiersByFaction(Faction.FreeWorldsLeague).ConfigureAwait(false);
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus",
                ExSLDF = false
            };
            var modifiedRoll = await _generator.ApplyRATModifier(12, combatCommand.Name, combatCommand.Allegiance).ConfigureAwait(false);

            Assert.AreEqual(15, modifiedRoll);
        }

        [Test]
        public async Task ApplyRatModifierJson_Avalon_Hussar_EdgeCase()
        {
            var table = await _generator.GetRATModifiersByFaction(Faction.FreeWorldsLeague).ConfigureAwait(false);
            var combatCommand = new CombatCommand
            {
                Name = "56th Avalon Hussars",
                Allegiance = Faction.FederatedSuns,
                Experience = Experience.Green,
                Loyalty = Loyalty.Reliable,
                Weight = Weight.Heavy,
                Strength = 133,
                Location = "Towne",
                ExSLDF = false
            };
            var modifiedRoll = await _generator.ApplyRATModifier(12, combatCommand.Name, combatCommand.Allegiance).ConfigureAwait(false);

            Assert.AreEqual(15, modifiedRoll);
        }

        [Test]
        public async Task ApplyRatModifierJson_Avalon_Hussar_EdgeCase2()
        {
            var table = await _generator.GetRATModifiersByFaction(Faction.FreeWorldsLeague).ConfigureAwait(false);
            var combatCommand = new CombatCommand
            {
                Name = "13th Avalon Hussars",
                Allegiance = Faction.FederatedSuns,
                Experience = Experience.Regular,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 133,
                Location = "Xhosa VII",
                ExSLDF = false
            };
            var modifiedRoll = await _generator.ApplyRATModifier(12, combatCommand.Name, combatCommand.Allegiance).ConfigureAwait(false);

            Assert.AreEqual(16, modifiedRoll);
        }

        [Test]
        public async Task GenerateRandomMech()
        {
            var weight = Weight.Medium;
            var faction = Faction.FreeWorldsLeague;
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus",
                ExSLDF = false
            };

            var mech = await _generator.Roll(weight, faction, combatCommand).ConfigureAwait(false);
            Assert.IsNotNull(mech);
        }
    }
}
