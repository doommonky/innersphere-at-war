﻿using System;
using System.Collections.Generic;
using System.Text;
using ISAW.Core;
using NUnit.Framework;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace ISAW.Test
{
    [TestFixture]
    public class ElementGeneratorTests : TestBase
    {
        private IElementGenerator<Lance, Element> _generator;
        public ElementGeneratorTests()
        {
        }

        [SetUp]
        public void Setup()
        {
            _generator = _serviceProvider.GetService<IElementGenerator<Lance, Element>>();
        }

        [Test]
        public void GenerateNonExSLDFLance_ReturnsFourIdentical()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus",
                ExSLDF = false
            };
            var lance = new Lance
            {
                Id = Guid.NewGuid(),
                Type = CompositionType.Battlemech,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
            };

            var elements = _generator.Generate(lance, combatCommand).ToArray();

            Assert.AreEqual(4, elements.Length);
            Assert.IsTrue(elements.All(mech => mech.ParentId == elements[0].ParentId));
            Assert.IsTrue(elements.All(mech => mech.Name == elements[0].Name));
        }

        [Test]
        public void GenerateExSLDFLance_ReturnsFourDifferent()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus",
                ExSLDF = true 
            };
            var lance = new Lance
            {
                Id = Guid.NewGuid(),
                Type = CompositionType.Battlemech,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
            };

            var elements = _generator.Generate(lance, combatCommand).ToArray();

            Assert.AreEqual(4, elements.Length);
            Assert.IsTrue(elements.All(mech => mech.ParentId == elements[0].ParentId));
            Assert.IsFalse(elements.All(mech => mech.Name == elements[0].Name));
        }
    }
}
