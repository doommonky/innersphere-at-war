﻿using NUnit.Framework;
using ISAW.Core;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace ISAW.Test
{
    [TestFixture]
    public class CombatValuesGeneratorTests : TestBase
    {
        private ICombatValuesGenerator _combatValuesGenerator;
        [SetUp]
        public void SetUp()
        {
            _combatValuesGenerator = _serviceProvider.GetService<ICombatValuesGenerator>();
        }

        [Test]
        public void CappellanLightInfantryRegimentCombatValues()
        {
            var result = _combatValuesGenerator.GenerateCombatValues(Guid.NewGuid(), ElementType.Regiment, Faction.CapellanConfederation, CompositionType.Infantry, Weight.Light);

            Assert.IsNotNull(result);
            Assert.IsNull(result.Jump);
            Assert.IsNull(result.EAero);
            Assert.AreEqual(6, result.TransportMove);
            Assert.AreEqual(1, result.Move);
            Assert.AreEqual(3, result.TMM);
            Assert.AreEqual(18, result.ARM);
            Assert.AreEqual(6, result.Short);
            Assert.AreEqual(3, result.Medium);
            Assert.AreEqual(0, result.Long);
            Assert.AreEqual(48, result.PointsValue);
        }

        [Test]
        public void CappellanAssaultBattlemechBattalionCombatValues()
        {
            var result = _combatValuesGenerator.GenerateCombatValues(Guid.NewGuid(), ElementType.Battalion, Faction.CapellanConfederation, CompositionType.Battlemech, Weight.Assault);

            Assert.IsNotNull(result);
            Assert.IsNull(result.Jump);
            Assert.IsNull(result.EAero);
            Assert.IsNull(result.TransportMove);
            Assert.AreEqual(5, result.Move);
            Assert.AreEqual(3, result.TMM);
            Assert.AreEqual(48, result.ARM);
            Assert.AreEqual(12, result.Short);
            Assert.AreEqual(13, result.Medium);
            Assert.AreEqual(5, result.Long);
            Assert.AreEqual(134, result.PointsValue);
        }

        [Test]
        public void MercinariesMediumAerospaceCompanyCombatValues()
        {
            var result = _combatValuesGenerator.GenerateCombatValues(Guid.NewGuid(), ElementType.Company, Faction.Mercenaries, CompositionType.Aerospace, Weight.Medium);

            Assert.IsNotNull(result);
            Assert.IsNull(result.Jump);
            Assert.IsNull(result.TransportMove);
            Assert.AreEqual(6, result.Move);
            Assert.AreEqual(2, result.TMM);
            Assert.AreEqual(7, result.ARM);
            Assert.AreEqual(2, result.Short);
            Assert.AreEqual(2, result.Medium);
            Assert.AreEqual(1, result.Long);
            Assert.AreEqual(0, result.EAero);
            Assert.AreEqual(19, result.PointsValue);
        }
    }
}
