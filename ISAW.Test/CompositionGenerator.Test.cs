﻿using ISAW.Core;
using NUnit.Framework;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace ISAW.Test
{
    [TestFixture]
    class CompositionGeneratorTest : TestBase
    {
        private ICompositionGenerator _compositionGenerator;
        [SetUp]
        public void Setup()
        {
            _compositionGenerator = _serviceProvider.GetService<ICompositionGenerator>();
        }

        [Test]
        public void CappellanInfantryBattalionComposition()
        {
            var result = _compositionGenerator.GetCompositionFor(Faction.CapellanConfederation, ElementType.Battalion, CompositionType.Infantry, Weight.Light);

            Assert.IsNotNull(result);
            Assert.AreEqual(5, result.Count());
            Assert.AreEqual(2, result.Where(r => r.Type == CompositionType.Infantry).Count());
            Assert.AreEqual(3, result.Where(r => r.Type == CompositionType.InfantryTransport).Count());
        }

        [Test]
        public void CappellanMediumBattlemechBattalionComposition()
        {
            var result = _compositionGenerator.GetCompositionFor(Faction.CapellanConfederation, ElementType.Battalion, CompositionType.Battlemech, Weight.Medium);

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count());
            Assert.AreEqual(1, result.Where(r => r.Weight == Weight.Light).Count());
            Assert.AreEqual(2, result.Where(r => r.Weight == Weight.Medium).Count());
        }
    }
}
