﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using ISAW.Core;

namespace ISAW.Test
{
    [TestFixture]
    public class TestBase
    {
        protected ServiceProvider _serviceProvider { get; set; }

        public TestBase()
        {
            _serviceProvider = BuildServiceProvider();
        }

        private ServiceProvider BuildServiceProvider()
        {
            var services = new ServiceCollection();
            
            services.AddSingleton<ICombatCommandCompositions, CombatCommandCompositions>();
            services.AddSingleton<IRandomBattalionsCompositionsGenerator, RandomBattalionCompositionsGenerator>();
            services.AddSingleton<IRandomLanceCompositions, RandomLanceCompositions>();

            //Generator Services
            services.AddSingleton<ICombatCommandCompositionGenerator, CombatCommandCompositionGenerator>();
            services.AddSingleton<ICombatValuesGenerator, CombatValuesGenerator>();
            services.AddSingleton<ICompositionGenerator, CompositionGenerator>();
            services.AddSingleton<IRATGenerator, RATGenerator>();

            services.AddTransient<IElementGenerator<CombatCommand, Regiment>, RegimentGenerator>();
            services.AddTransient<IElementGenerator<Regiment, Battalion>, BattalionGenerator>();
            services.AddTransient<IElementGenerator<Battalion, Company>, CompanyGenerator>();
            services.AddTransient<IElementGenerator<Company, Lance>, LanceGenerator>();
            services.AddTransient<IElementGenerator<Lance, Element>, ElementGenerator>();

            return services.BuildServiceProvider();
        }
    }
}
