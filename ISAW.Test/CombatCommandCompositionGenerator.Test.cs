using System.Linq;
using NUnit.Framework;
using ISAW.Core;
using Microsoft.Extensions.DependencyInjection;

namespace ISAW.Test
{
    [TestFixture]
    public class CombatCommandCompositionGeneratorTests : TestBase
    {
        private ICombatCommandCompositionGenerator _generator;

        [SetUp]
        public void SetUp()
        {
            _generator = _serviceProvider.GetService<ICombatCommandCompositionGenerator>();
        }

        [Test]
        public void GenerateFreeWorldsLeagueCombatCommandComposition()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus"
            };

            _generator.GenerateCombatCommandComposition(combatCommand);

            Assert.IsNotEmpty(combatCommand.Regiments);
            Assert.AreEqual(1, combatCommand.Regiments.Where(r => r.Type == CompositionType.Battlemech).Count());
            Assert.AreEqual(2, combatCommand.Regiments.Where(r => r.Type == CompositionType.Aerospace).Count());
            Assert.AreEqual(3, combatCommand.Regiments.Where(r => r.Type == CompositionType.Armor).Count());
            Assert.AreEqual(8, combatCommand.Regiments.Where(r => r.Type == CompositionType.Infantry).Count());
            Assert.AreEqual(1, combatCommand.Regiments.Where(r => r.Type == CompositionType.Artillery).Count());
        }

        [Test]
        public void GenerateDraconisCombineCombatCommandComposition()
        {
            var combatCommand = new CombatCommand
            {
                Name = "11th Alshain Avengers",
                Allegiance = Faction.DraconisCombine,
                Experience = Experience.Legendary,
                Loyalty = Loyalty.Questionable,
                Weight = Weight.Assault,
                Strength = 100,
                Location = "Alshain"
            };
            _generator.GenerateCombatCommandComposition(combatCommand);

            Assert.IsNotEmpty(combatCommand.Regiments);
            Assert.AreEqual(1, combatCommand.Regiments.Where(r => r.Type == CompositionType.Battlemech).Count());
            Assert.AreEqual(3, combatCommand.Regiments.Where(r => r.Type == CompositionType.Aerospace).Count());
            Assert.AreEqual(3, combatCommand.Regiments.Where(r => r.Type == CompositionType.Armor).Count());
            Assert.AreEqual(5, combatCommand.Regiments.Where(r => r.Type == CompositionType.Infantry).Count());
            Assert.AreEqual(1, combatCommand.Regiments.Where(r => r.Type == CompositionType.Artillery).Count());
        }

        [Test]
        public void GenerateCappellanCombatCommandComposition()
        {
            var combatCommand = new CombatCommand
            {
                Name = "Some Cappellan Combat Command",
                Allegiance = Faction.CapellanConfederation,
                Experience = Experience.WetBehindTheEars,
                Loyalty = Loyalty.Questionable,
                Weight = Weight.Light,
                Strength = 100,
                Location = "Someplace"
            };
            _generator.GenerateCombatCommandComposition(combatCommand);

            Assert.IsNotEmpty(combatCommand.Regiments);
            Assert.AreEqual(1, combatCommand.Regiments.Where(r => r.Type == CompositionType.Battlemech).Count());
            Assert.AreEqual(2, combatCommand.Regiments.Where(r => r.Type == CompositionType.Aerospace).Count());
            Assert.AreEqual(3, combatCommand.Regiments.Where(r => r.Type == CompositionType.Armor).Count());
            Assert.AreEqual(5, combatCommand.Regiments.Where(r => r.Type == CompositionType.Infantry).Count());
            Assert.AreEqual(2, combatCommand.Regiments.Where(r => r.Type == CompositionType.Artillery).Count());
        }
    }
}