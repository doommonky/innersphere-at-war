﻿using ISAW.Core;
using NUnit.Framework;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace ISAW.Test
{
    [TestFixture]
    class CompanyGeneratorTest : TestBase
    {
        private IElementGenerator<Battalion, Company> _companies;
        [SetUp]
        public void Setup()
        {
            _companies = _serviceProvider.GetService<IElementGenerator<Battalion, Company>>();
        }

        [Test]
        public void CompanyGenerator_FWLMediumBattlemechBattalion_Returns3MediumCompanies()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus"
            };
            var battalion = new Battalion 
            {
                Type = CompositionType.Battlemech,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
            };

            var companies = _companies.Generate(battalion, combatCommand);

            Assert.AreEqual(3, companies.Count());
            Assert.AreEqual(3, companies.Where(c => c.Weight == Weight.Medium).Count());
        }

        [Test]
        public void CompanyGenerator_FWLAssaultBattlemechBattalion_Returns1A1H1LCompanies()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus"
            };
            var battalion = new Battalion
            {
                Type = CompositionType.Battlemech,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Assault,
            };

            var companies = _companies.Generate(battalion, combatCommand);

            Assert.AreEqual(3, companies.Count());
            Assert.AreEqual(1, companies.Where(c => c.Weight == Weight.Medium).Count());
            Assert.AreEqual(1, companies.Where(c => c.Weight == Weight.Heavy).Count());
            Assert.AreEqual(1, companies.Where(c => c.Weight == Weight.Assault).Count());
        }

        [Test]
        public void BattalionGenerator_OverstrengthCC_ReturnsNormalArmorBattalions()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 133,
                Location = "Atreus"
            };
            var battalion = new Battalion 
            {
                Type = CompositionType.Armor,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Assault,
            };

            var companies = _companies.Generate(battalion, combatCommand);

            Assert.AreEqual(3, companies.Count());
            Assert.AreEqual(2, companies.Where(c => c.Weight == Weight.Assault).Count());
            Assert.AreEqual(1, companies.Where(c => c.Weight == Weight.Heavy).Count());
        }

        [Test]
        public void CompanyGenerator_GenerateInfantryCompanies()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 133,
                Location = "Atreus"
            };
            var battalion = new Battalion 
            {
                Type = CompositionType.Infantry,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Light,
            };

            var companies = _companies.Generate(battalion, combatCommand);

            Assert.AreEqual(5, companies.Count());
            Assert.IsTrue(companies.Where(b => b.Type == CompositionType.Infantry).Count() == 2);
            Assert.IsTrue(companies.Where(c => c.Type == CompositionType.InfantryTransport).Count() == 3);
        }
    }
}
