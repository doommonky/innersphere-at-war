using System.Linq;
using NUnit.Framework;
using ISAW.Core;
using Microsoft.Extensions.DependencyInjection;

namespace ISAW.Test
{
    public class RegimentCompositionGeneratorTests : TestBase
    {
        private IElementGenerator<CombatCommand, Regiment> _generator;
        public RegimentCompositionGeneratorTests()
        {
        }

        [SetUp]
        public void Setup()
        {
            _generator = _serviceProvider.GetService<IElementGenerator<CombatCommand, Regiment>>();
        }

        [Test]
        public void GenerateFreeWorldsLeagueRegimentComposition()
        {
            var combatCommand = new CombatCommand 
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus"
            };
            var regiments = _generator.Generate(combatCommand);

            Assert.IsNotEmpty(regiments);
            Assert.AreEqual(1, regiments.Where(r => r.Type == CompositionType.Battlemech).Count());
            Assert.AreEqual(2, regiments.Where(r => r.Type == CompositionType.Aerospace).Count());
            Assert.AreEqual(3, regiments.Where(r => r.Type == CompositionType.Armor).Count());
            Assert.AreEqual(8, regiments.Where(r => r.Type == CompositionType.Infantry).Count());
            Assert.AreEqual(1, regiments.Where(r => r.Type == CompositionType.Artillery).Count());
        }

        [Test]
        public void GenerateDraconisCombineRegimentComposition()
        {
            var combatCommand = new CombatCommand
            {
                Name = "11th Alshain Avengers",
                Allegiance = Faction.DraconisCombine,
                Experience = Experience.Legendary,
                Loyalty = Loyalty.Questionable,
                Weight = Weight.Assault,
                Strength = 100,
                Location = "Alshain"
            };
            var regiments = _generator.Generate(combatCommand);

            Assert.IsNotEmpty(regiments);
            Assert.AreEqual(1, regiments.Where(r => r.Type == CompositionType.Battlemech).Count());
            Assert.AreEqual(3, regiments.Where(r => r.Type == CompositionType.Aerospace).Count());
            Assert.AreEqual(3, regiments.Where(r => r.Type == CompositionType.Armor).Count());
            Assert.AreEqual(5, regiments.Where(r => r.Type == CompositionType.Infantry).Count());
            Assert.AreEqual(1, regiments.Where(r => r.Type == CompositionType.Artillery).Count());
        }

        [Test]
        public void GenerateCappellanRegimentComposition()
        {
            var combatCommand = new CombatCommand 
            {
                Name = "Some Cappellan Combat Command",
                Allegiance = Faction.CapellanConfederation,
                Experience = Experience.WetBehindTheEars,
                Loyalty = Loyalty.Questionable,
                Weight = Weight.Light,
                Strength = 100,
                Location = "Someplace"
            };
            var regiments = _generator.Generate(combatCommand);

            Assert.IsNotEmpty(regiments);
            Assert.AreEqual(1, regiments.Where(r => r.Type == CompositionType.Battlemech).Count());
            Assert.AreEqual(2, regiments.Where(r => r.Type == CompositionType.Aerospace).Count());
            Assert.AreEqual(3, regiments.Where(r => r.Type == CompositionType.Armor).Count());
            Assert.AreEqual(5, regiments.Where(r => r.Type == CompositionType.Infantry).Count());
            Assert.AreEqual(2, regiments.Where(r => r.Type == CompositionType.Artillery).Count());
        }

        [Test]
        public void LyranAssaultCombatCommand_GenerateRegiments_ReturnsHeavyAerospaceRegiments()
        {
            var combatCommand = new CombatCommand
            {
                Name = "Lyran Assholes",
                Allegiance = Faction.LyranCommonwealth,
                Experience = Experience.Heroic,
                Loyalty = Loyalty.Questionable,
                Weight = Weight.Assault,
                Strength = 100,
                Location = "Earth"
            };

            var regiments = _generator.Generate(combatCommand);

            Assert.IsNotNull(regiments);
            Assert.IsNotEmpty(regiments);
            Assert.AreEqual(Weight.Heavy, regiments.FirstOrDefault(r => r.Type == CompositionType.Aerospace).Weight);
        }
    }
}