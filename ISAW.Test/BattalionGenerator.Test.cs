﻿using ISAW.Core;
using NUnit.Framework;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace ISAW.Test
{
    [TestFixture]
    class BattalionGeneratorTest : TestBase
    {
        private IElementGenerator<Regiment, Battalion> _battalionGenerator;

        [SetUp]
        public void Setup()
        {
            _battalionGenerator = _serviceProvider.GetService<IElementGenerator<Regiment, Battalion>>();
        }

        [Test]
        public void BattalionGenerator_RegularStrengthCC_ReturnsExtraBattlemechBattalion()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus"
            };
            var regiment = new Regiment
            {
                Type = CompositionType.Battlemech,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
            };

            var battalions = _battalionGenerator.Generate(regiment, combatCommand);

            Assert.AreEqual(3, battalions.Count());
        }

        [Test]
        public void BattalionGenerator_OverstrengthCC_ReturnsExtraBattlemechBattalion()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 133,
                Location = "Atreus"
            };
            var regiment = new Regiment
            {
                Type = CompositionType.Battlemech,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
            };

            var battalions = _battalionGenerator.Generate(regiment, combatCommand);

            Assert.AreEqual(4, battalions.Count());
        }

        [Test]
        public void BattalionGenerator_OverstrengthCC_ReturnsNormalArmorBattalions()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 133,
                Location = "Atreus"
            };
            var regiment = new Regiment
            {
                Type = CompositionType.Armor,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
            };

            var battalions = _battalionGenerator.Generate(regiment, combatCommand);

            Assert.AreEqual(3, battalions.Count());
        }

        [Test]
        public void BattalionGenerator_GenerateInfantryBattalions()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 133,
                Location = "Atreus"
            };
            var regiment = new Regiment
            {
                Type = CompositionType.Infantry,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Light,
            };

            var battalions = _battalionGenerator.Generate(regiment, combatCommand);

            Assert.AreEqual(3, battalions.Count());
            Assert.IsTrue(battalions.Where(b => b.Type == CompositionType.Infantry).Count() == 3);
            Assert.IsTrue(battalions.Where(b => b.Weight == Weight.Light).Count() == 3);
        }

        [Test]
        public void BattalionGenerator_WeightFromRegiment()
        {
            var combatCommand = new CombatCommand
            {
                Name = "3rd Brandon Bums",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Assault,
                Strength = 100,
                Location = "Grayslake"
            };
            var regiment = new Regiment
            {
                Type = CompositionType.Aerospace,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Heavy
            };

            var battalions = _battalionGenerator.Generate(regiment, combatCommand);

            Assert.AreEqual(1, battalions.Count());
            Assert.AreEqual(1, battalions.Where(b => b.Weight == Weight.Heavy).Count());
        }
    }
}
