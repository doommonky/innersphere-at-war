﻿using ISAW.Core;
using NUnit.Framework;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace ISAW.Test
{
    [TestFixture]
    class LanceGeneratorTest : TestBase
    {
        private IElementGenerator<Company, Lance> _lances;
        [SetUp]
        public void Setup()
        {
            _lances = _serviceProvider.GetService<IElementGenerator<Company, Lance>>();
        }

        [Test]
        public void LanceGenerator_FWLMediumBattlemechCompany_Returns2M1LLances()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus"
            };
            var company = new Company 
            {
                Type = CompositionType.Battlemech,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
            };

            var lances = _lances.Generate(company, combatCommand);

            Assert.AreEqual(3, lances.Count());
            Assert.AreEqual(2, lances.Where(c => c.Weight == Weight.Medium).Count());
            Assert.AreEqual(1, lances.Where(c => c.Weight == Weight.Light).Count());
        }

        [Test]
        public void LanceGenerator_FWLHeavyBattlemechCompany_Returns1A2HLances()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 100,
                Location = "Atreus"
            };
            var company = new Company
            {
                Type = CompositionType.Battlemech,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Heavy,
            };

            var lances = _lances.Generate(company, combatCommand);

            Assert.AreEqual(3, lances.Count());
            Assert.AreEqual(2, lances.Where(c => c.Weight == Weight.Heavy).Count());
            Assert.AreEqual(1, lances.Where(c => c.Weight == Weight.Assault).Count());
        }

        [Test]
        public void CompanyGenerator_OverstrengthCC_ReturnsNormalArmorCompanys()
        {
            var combatCommand = new CombatCommand
            {
                Name = "1st Atrean Dragoons",
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Medium,
                Strength = 133,
                Location = "Atreus"
            };
            var company = new Company 
            {
                Type = CompositionType.Armor,
                Allegiance = Faction.FreeWorldsLeague,
                Experience = Experience.Veteran,
                Loyalty = Loyalty.Fanatical,
                Weight = Weight.Assault,
            };

            var lances = _lances.Generate(company, combatCommand);

            Assert.AreEqual(3, lances.Count());
            Assert.AreEqual(2, lances.Where(c => c.Weight == Weight.Assault).Count());
            Assert.AreEqual(1, lances.Where(c => c.Weight == Weight.Heavy).Count());
        }
    }
}
