export function typeMap(typeId){
    switch(typeId){
        case 0:
            return "Battlemech";
        case 1:
            return "Aerospace";
        case 2:
            return "Armor";
        case 3:
            return "Infantry";
        case 4: 
            return "Infantry Transport";
        case 5:
            return "Artillery";
        case 6:
            return "Dropship";
        default:
            return "Unknown";
    }
}

export function weightMap(weightId){
    switch(weightId){
        case 0:
            return "Light";
        case 1:
            return "Medium";
        case 2:
            return "Heavy";
        case 3:
            return "Assault";
        default:
            return "Unknown";
    }
}

export function factionMap(allegianceId){
    switch(allegianceId){
        case 0:
            return "Capellan Confederation";
        case 1:
            return "Draconis Combine";
        case 2: 
            return "Federated Suns";
        case 3:
            return "Free Worlds League";
        case 4:
            return "Lyran Commonwealth";
        case 5: 
            return "Magistracy of Canopus";
        case 6:
            return "Outworlds Alliance";
        case 7:
            return "Taurian Concordat";
        case 8:
            return "Mercenaries";
        case 10:
            return "Comstar";
        default:
            return "Unknown";
    }
}

export function experienceMap(experienceId){
    switch(experienceId){
        case 0: 
            return "Legendary";
        case 1:
            return "Heroic";
        case 2:
            return "Elite";
        case 3:
            return "Veteran";
        case 4:
            return "Regular";
        case 5:
            return "Green";
        case 6:
            return "Really Green";
        case 7:
            return "Wet Behind the Ears";
        default:
            return "Unknown";
    }
}

export function loyaltyMap(loyaltyId){
    switch(loyaltyId){
        case 0:
            return "Questionable";
        case 1:
            return "Reliable";
        case 2:
            return "Fanatical";
        default:
            return "Unknown";
    }
}