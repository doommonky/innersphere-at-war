import React, { useState } from "react";
import axios from "axios";

export default function Uploader(props) {
	const [file, setFile] = useState();
	const [inProgress, setInProgress] = useState(false);
	const [complete, setComplete] = useState(false);
    const [error, setError] = useState(null);

	const saveFile = (e) => {
        setInProgress(false);
        setComplete(false);
		setFile(e.target.files[0]);
	};

	const uploadFile = async (e) => {
        setError(false);
		setInProgress(true);
		const formData = new FormData();
		formData.append("file", file);
		try {
			await axios
				.post("https://localhost:44351/generations/upload", formData)
				.then((result) => {
					setInProgress(false);
					setComplete(true);
				});
		} catch (ex) {
            setError("Something has gone wrong. Please try your request again.");
            setInProgress(false);
			console.log(ex);
		}
	};

	return (
		<div>
            {error && <p>{error}</p>}
            {complete && <p>Element generation successful.</p>}
            {inProgress && <p>Element generation in progress. Please wait...</p>}
			{!inProgress && <div>
				<input type="file" onChange={saveFile}/>
				<input type="button" value="Upload" onClick={uploadFile} />
			</div>}
		</div>
	);
}
