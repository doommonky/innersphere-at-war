import React from "react";
import styles from "../components/componentstyle.module.css"

export default function FactionSelector(props) {
	return (
		<div className={styles.componentDiv}>
            <h1>Battletech: Innersphere at War</h1>
			<div style={{display: 'flex', flexDirection: 'row', flexWrap:'wrap', justifyContent: 'space-evenly', alignItems: 'center', alignContent: 'space-evenly', rowGap:'25px', columnGap:'25px', margin:'25px', height:'100%'}}>
				<button
					onClick={() => props.selectFaction(0)}
					style={{ backgroundColor: "darkgrey" }}
				>
					<img
						src="../logos/Capellan Confederation.png"
						alt="Capellan Confederation"
						style={{ height: "200px" }}
					/>
				</button>
				<button
					onClick={() => props.selectFaction(1)}
					style={{ backgroundColor: "darkgray" }}
				>
					<img
						src="../logos/Draconis Combine.png"
						alt="Draconis Combine"
						style={{ height: "200px"}}
					/>
				</button>
                <button
					onClick={() => props.selectFaction(2)}
					style={{ backgroundColor: "darkgray" }}
				>
					<img
						src="../logos/Federated Suns.png"
						alt="Federated Suns"
						style={{ height: "200px"}}
					/>
				</button>
                <button
					onClick={() => props.selectFaction(3)}
					style={{ backgroundColor: "darkgray" }}
				>
					<img
						src="../logos/Free Worlds League.png"
						alt="Free Worlds League"
						style={{ height: "200px" }}
					/>
				</button>
                <button
					onClick={() => props.selectFaction(4)}
					style={{ backgroundColor: "darkgray" }}
				>
					<img
						src="../logos/Lyran Commonwealth.png"
						alt="Lyran Commonwealth"
						style={{ height: "200px"}}
					/>
				</button>
				<button
					onClick={() => props.selectFaction(5)}
					style={{ backgroundColor: "darkgray" }}
				>
					<img
						src="../logos/Magistracy of Canopus.png"
						alt="Magistracy of Canopus"
						style={{ height: "200px"}}
					/>
				</button>
				<button
					onClick={() => props.selectFaction(6)}
					style={{ backgroundColor: "darkgray" }}
				>
					<img
						src="../logos/Outworlds Alliance.png"
						alt="Outworlds Alliance"
						style={{ height: "200px" }}
					/>
				</button>
                <button
					onClick={() => props.selectFaction(7)}
					style={{ backgroundColor: "darkgray" }}
				>
					<img
						src="../logos/Taurian Concordat.png"
						alt="Taurian Concordat"
						style={{ height: "200px"}}
					/>
				</button>
                <button
					onClick={() => props.selectFaction(8)}
					style={{ backgroundColor: "darkgray" }}
				>
					<img
						src="../logos/Mercenaries.png"
						alt="Mercinaries"
						style={{ height: "200px"}}
					/>
				</button>
                <button
					onClick={() => props.selectFaction(10)}
					style={{ backgroundColor: "darkgray" }}
				>
					<img
						src="../logos/Comstar.png"
						alt="Comstar"
						style={{ height: "200px"}}
					/>
				</button>
			</div>
		</div>
	);
}
