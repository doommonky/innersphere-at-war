import React, {useState} from 'react';
import {factionMap} from '../utilities/mappers'
import FactionSelector from './FactionSelector'
import FactionHome from './FactionHome'

export default function Home(props){
    const [selectedFaction, setSelectedFaction] = useState(undefined);

    function selectFaction(faction){
        setSelectedFaction(faction);
    }

    return(
    <div style={{height: '100%', backgroundImage:'../victor.jpg', backgroundColor:'#181818', color:'lightgrey'}}>
        {selectedFaction == null && <FactionSelector 
            selectFaction={selectFaction}/>}
        {selectedFaction >= 0 && <FactionHome
            faction={factionMap(selectedFaction)}
            clearFaction={selectFaction}/>}
    </div> 
    )
}