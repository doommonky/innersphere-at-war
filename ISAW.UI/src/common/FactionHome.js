import React, { useState, useEffect } from "react";
import axios from "axios";
import styles from "../components/componentstyle.module.css";
import CombatCommand from "../components/CombatCommand";
import Uploader from "./Uploader";

export default function FactionHome(props) {
	const [isLoaded, setIsLoaded] = useState(false);
	const [data, setData] = useState(null);

	useEffect(() => {
		if (props.faction === "Comstar")
			setIsLoaded(true);
		else if (!isLoaded) {
			axios
				.get(
					`https://localhost:44351/combatcommands?allegiance=${props.faction.replace(
						/ /g,
						""
					)}`
				)
				.then((response) => {
					setData(response.data);
				});
			setIsLoaded(true);
		}
	}, [isLoaded, props]);

	function handleBackClick() {
		setIsLoaded(false);
		props.clearFaction();
	}

	return (
		<div>
			<button onClick={handleBackClick}>
				<img
					src="../backArrow.png"
					alt="Back"
					style={{ height: "20px", width: "15px" }}
				/>
			</button>
			<div className={styles.componentDiv}>
				<h1>{props.faction}</h1>
				<img
					style={{
						display: "flex",
						flexDirection: "column",
						height: "200px",
						justifyContent: "center",
					}}
					src={`./logos/${props.faction}.png`}
					alt={props.faction}
				></img>
			</div>
			<div className={styles.componentDiv}>
                {!data && props.faction !== "Comstar" && <p>Loading...</p>}
				{!data && props.faction === "Comstar" && <Uploader/>}
				{data &&
					data.map((combatCommand) => {
						return (
							<CombatCommand
								{...combatCommand}
								key={combatCommand.id}
							/>
						);
					})}
				{data && data.length === 0 && (
					<p>
						This faction has no currently generated combat command
						elements.
					</p>
				)}
			</div>
		</div>
	);
}
