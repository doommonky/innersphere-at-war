import React, { useState, useEffect } from "react";
import Battalion from "./Battalion";
import CombatValues from "./CombatValues";
import ElementValues from "./ElementValues";
import { typeMap } from "../utilities/mappers";
import axios from "axios";

import styles from "./componentstyle.module.css";

export default function Regiment(props) {
	const [isLoaded, setIsLoaded] = useState(false);
	const [battalionsOpen, setBattalionsOpen] = useState(false);
	const [battalions, setBattalions] = useState();
	const [combatValues, setCombatValues] = useState();

	useEffect(() => {
		if (!isLoaded) {
			axios
				.get(
					`https://localhost:44351/combatvalues/parent/${props.id}`
				)
				.then((response) => {
					setCombatValues(response.data);
				});
			setIsLoaded(true);
		}
	}, [isLoaded, props]);

	function handleBattalionsClick() {
		if (!battalions) {
			axios
				.get(`https://localhost:44351/battalions/parent/${props.id}`)
				.then((response) => {
					setBattalions(response.data);
				})
				.then(() => toggleBattalionsOpen());
		} else {
			toggleBattalionsOpen();
		}
	}
	function toggleBattalionsOpen() {
		setBattalionsOpen(!battalionsOpen);
	}

	return (
		<div className={styles.combatElementColumn}>
			<h2>{typeMap(props.type)} Regiment</h2>
			<ElementValues
				type={props.type}
				allegiance={props.allegiance}
				experience={props.experience}
				loyalty={props.loyalty}
				weight={props.weight}
			/>
			{combatValues && <CombatValues {...combatValues} />}
			<button
				className={styles.button}
				onClick={() => handleBattalionsClick()}
			>
				Battalions
			</button>
			{battalionsOpen &&
				battalions?.map((battalion) => {
					return (
						battalion && (
							<Battalion {...battalion} key={battalion.id} />
						)
					);
				})}
		</div>
	);
}
