import React from "react";
import {
	factionMap,
	experienceMap,
	loyaltyMap,
	weightMap,
	typeMap,
} from "../utilities/mappers";
import styles from "./componentstyle.module.css";

export default function ElementValues(props) {
	return (
		<div className={styles.componentDiv}>
			<table className={styles.table}>
				<tbody>
					<tr>
						{props.type >= 0 && <th className={styles.table}>Type</th>}
						<th className={styles.table}>Allegiance</th>
						{props.activeContract >= 0 &&
						props.allegiance !== props.activeContract &&
							<th className={styles.table}>Active Contract</th>}
						<th className={styles.table}>Experience</th>
						<th className={styles.table}>Loyalty</th>
						<th className={styles.table}>Size</th>
						{props.strength && (
							<th className={styles.table}>Strength</th>
						)}
						{props.location && (
							<th className={styles.table}>Location</th>
						)}
					</tr>
					<tr>
						{props.type >= 0 && <td className={styles.table}>{typeMap(props.type)}</td>}
						<td className={styles.table}>
							{factionMap(props.allegiance)}
						</td>
						{props.activeContract >= 0 &&
						props.allegiance !== props.activeContract &&
							<td>{factionMap(props.activeContract)}</td>}
						<td className={styles.table}>
							{experienceMap(props.experience)}
						</td>
						<td className={styles.table}>
							{loyaltyMap(props.loyalty)}
						</td>
						<td className={styles.table}>
							{weightMap(props.weight)}
						</td>
						{props.strength && (
							<td className={styles.table}>
								{props.strength + "%"}
							</td>
						)}
						{props.location && (
							<td className={styles.table}>{props.location}</td>
						)}
					</tr>
				</tbody>
			</table>
		</div>
	);
}
