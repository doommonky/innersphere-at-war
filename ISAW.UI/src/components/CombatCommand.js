import React, { useState } from "react";
import Regiment from "./Regiment";
import styles from "./componentstyle.module.css";
import ElementValues from "./ElementValues";
import axios from "axios";

export default function CombatCommand(props) {
	const [regimentsOpen, setRegimentsOpen] = useState(false);
	const [regiments, setRegiments] = useState();

	function handleRegimentsClick() {
		if (!regiments) {
			axios
				.get(`https://localhost:44351/regiments/parent/${props.id}`)
				.then((response) => {
					setRegiments(response.data);
				}).then(() => toggleRegimentsOpen());
		}
		else{
			toggleRegimentsOpen();
		}
	}
	function toggleRegimentsOpen() {
		setRegimentsOpen(!regimentsOpen);
	}

	return (
		<div className={styles.componentDiv}>
			<h1>{props.name}</h1>
			<div className={styles.componentElementRow}>
				<ElementValues
					allegiance={props.allegiance}
					activeContract={props.activeContract}
					experience={props.experience}
					loyalty={props.loyalty}
					weight={props.weight}
					strength={props.strength}
					location={props.location}
				/>
				<button
					className={styles.button}
					onClick={() => handleRegimentsClick()}
				>
					Regiments
				</button>
				{regimentsOpen &&
					regiments.map((regiment) => {
						return (
							regiment && (
								<Regiment {...regiment} key={regiment.id} />
							)
						);
					})}
			</div>
		</div>
	);
}
