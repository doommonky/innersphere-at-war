import React, { useState, useEffect } from "react";
import CombatValues from "./CombatValues";
import styles from "./componentstyle.module.css";
import ElementValues from "./ElementValues";
import Element from "./Element";
import { typeMap } from "../utilities/mappers";
import axios from "axios";

export default function Lance(props) {
	const [isLoaded, setIsLoaded] = useState(false);
	const [elementsOpen, setElementsOpen] = useState(false);
	const [elements, setElements] = useState();
	const [combatValues, setCombatValues] = useState();

	useEffect(() => {
		if (!isLoaded) {
			axios
				.get(`https://localhost:44351/combatvalues/parent/${props.id}`)
				.then((response) => {
					setCombatValues(response.data);
				});
			setIsLoaded(true);
		}
	}, [isLoaded, props]);

	function handleElementsClick() {
		if (!elements) {
			axios
				.get(`https://localhost:44351/elements/parent/${props.id}`)
				.then((response) => {
					setElements(response.data);
				})
				.then(() => toggleElementsOpen());
		} else {
			toggleElementsOpen();
		}
	}

	function toggleElementsOpen() {
		setElementsOpen(!elementsOpen);
	}

	return (
		<div className={styles.combatElementColumn}>
			<h3>{typeMap(props.type)} Lance</h3>
			<ElementValues
				type={props.type}
				allegiance={props.allegiance}
				experience={props.experience}
				loyalty={props.loyalty}
				weight={props.weight}
			/>
			{combatValues && <CombatValues {...combatValues} />}
			<button
				className={styles.button}
				onClick={() => handleElementsClick()}
			>
				Elements
			</button>
			{elementsOpen &&
				elements?.map((element) => {
					return element && <Element {...element} key={element.id} />;
				})}
		</div>
	);
}
