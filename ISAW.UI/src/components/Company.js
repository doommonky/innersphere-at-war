import React, { useState, useEffect } from "react";
import CombatValues from "./CombatValues";
import styles from "./componentstyle.module.css";
import ElementValues from "./ElementValues";
import Lance from "./Lance";
import { typeMap } from "../utilities/mappers";
import axios from "axios";

export default function Company(props) {
	const [isLoaded, setIsLoaded] = useState(false);
	const [lancesOpen, setLancesOpen] = useState(false);
	const [lances, setLances] = useState();
	const [combatValues, setCombatValues] = useState();

	useEffect(() => {
		if (!isLoaded) {
			axios
				.get(`https://localhost:44351/combatvalues/parent/${props.id}`)
				.then((response) => {
					setCombatValues(response.data);
				});
			setIsLoaded(true);
		}
	}, [isLoaded, props]);

	function handleLancesClick() {
		if (!lances) {
			axios
				.get(`https://localhost:44351/lances/parent/${props.id}`)
				.then((response) => {
					setLances(response.data);
				})
				.then(() => toggleLancesOpen());
		} else {
			toggleLancesOpen();
		}
	}

	function toggleLancesOpen() {
		setLancesOpen(!lancesOpen);
	}

	return (
		<div className={styles.combatElementColumn}>
			<h3>{typeMap(props.type)} Company</h3>
			<ElementValues
				type={props.type}
				allegiance={props.allegiance}
				experience={props.experience}
				loyalty={props.loyalty}
				weight={props.weight}
			/>
			{combatValues && <CombatValues {...combatValues} />}
			<button
				className={styles.button}
				onClick={() => handleLancesClick()}
			>
				Lances
			</button>
			{lancesOpen &&
				lances?.map((lance) => {
					return lance && <Lance {...lance} key={lance.id} />;
				})}
		</div>
	);
}
