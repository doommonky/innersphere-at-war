import React, { useState, useEffect } from "react";
import CombatValues from "./CombatValues";
import styles from "./componentstyle.module.css";
import ElementValues from "./ElementValues";
import Company from "./Company";
import { typeMap } from "../utilities/mappers";
import axios from "axios";

export default function Battalion(props) {
	const [isLoaded, setIsLoaded] = useState(false);
	const [companiesOpen, setCompaniesOpen] = useState(false);
	const [companies, setCompanies] = useState();
	const [combatValues, setCombatValues] = useState();

	useEffect(() => {
		if (!isLoaded) {
			axios
				.get(`https://localhost:44351/combatvalues/parent/${props.id}`)
				.then((response) => {
					setCombatValues(response.data);
				});
			setIsLoaded(true);
		}
	}, [isLoaded, props]);

	function handleCompaniesClick() {
		if (!companies) {
			axios
				.get(`https://localhost:44351/companies/parent/${props.id}`)
				.then((response) => {
					setCompanies(response.data);
				})
				.then(() => toggleCompaniesOpen());
		} else {
			toggleCompaniesOpen();
		}
	}

	function toggleCompaniesOpen() {
		setCompaniesOpen(!companiesOpen);
	}

	return (
		<div className={styles.combatElementColumn}>
			<h3>{typeMap(props.type)} Battalion</h3>
			<ElementValues
				type={props.type}
				allegiance={props.allegiance}
				experience={props.experience}
				loyalty={props.loyalty}
				weight={props.weight}
			/>
			{combatValues && <CombatValues {...combatValues} />}
			<button
				className={styles.button}
				onClick={() => handleCompaniesClick()}
			>
				Companies
			</button>
			{companiesOpen &&
				companies?.map((company) => {
					return company && <Company {...company} key={company.id} />;
				})}
		</div>
	);
}
