import React, { useState, useEffect } from "react";
import CombatValues from "./CombatValues";
import styles from "./componentstyle.module.css";
import ElementValues from "./ElementValues";
import axios from "axios";

export default function Element(props) {
	const [isLoaded, setIsLoaded] = useState(false);
	const [combatValues, setCombatValues] = useState();

	useEffect(() => {
		if (!isLoaded) {
			axios
				.get(`https://localhost:44351/combatvalues/parent/${props.id}`)
				.then((response) => {
					setCombatValues(response.data);
				});
			setIsLoaded(true);
		}
	}, [isLoaded, props]);
	
	return (
		<div className={styles.combatElementColumn}>
			<h3>{props?.name ?? props.type + "Element"}</h3>
			<ElementValues
				type={props.type}
				allegiance={props.allegiance}
				experience={props.experience}
				loyalty={props.loyalty}
				weight={props.weight}
			/>
			{combatValues && <CombatValues {...combatValues} />}
		</div>
	);
}
