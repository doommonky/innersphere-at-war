import React from "react";
import styles from "./componentstyle.module.css";

export default function CombatValues(props) {
	return (
		<div className={styles.componentDiv}>
			<table className={styles.table}>
				<tbody>
					<tr>
						<th className={styles.table}>Move</th>
						{props.transportMove != null && (
							<th className={styles.table}>Transport Move</th>
						)}
						{props.jump != null && <th className={styles.table}>Jump</th>}
						<th className={styles.table}>TMM</th>
						<th className={styles.table}>ARM</th>
						<th className={styles.table}>Short</th>
						<th className={styles.table}>Medium</th>
						<th className={styles.table}>Long</th>
						{props.eAero != null && (
							<th className={styles.table}>E(Aero)</th> 
						)}
						<th className={styles.table}>Points Value</th>
						<th className={styles.table}>Special Rules</th>
					</tr>
					<tr>
						<td className={styles.table}>{props.move}</td>
						{props.transportMove != null && (
							<td className={styles.table}>
								{props.transportMove}
							</td>
						)}
						{props.jump != null && (
							<td className={styles.table}>{props.jump}</td>
						)}
						<td className={styles.table}>{props.tmm}</td>
						<td className={styles.table}>{props.arm}</td>
						<td className={styles.table}>{props.short}</td>
						<td className={styles.table}>{props.medium}</td>
						<td className={styles.table}>{props.long}</td>
						{props.eAero != null && (
							<td className={styles.table}>{props.eAero}</td>
						)}
						<td className={styles.table}>{props.pointsValue}</td>
						<td className={styles.table}>
							{props.specialRules.join(", ")}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	);
}
