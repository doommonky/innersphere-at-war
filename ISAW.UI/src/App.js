import Home from "./common/Home";

function App() {
  return (
    <div style={{fontSize: "12px", backgroundColor:"darkslategrey"}}>
      <Home />
    </div>
  );
}

export default App;
