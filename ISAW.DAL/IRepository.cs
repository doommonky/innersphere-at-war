﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ISAW.Core;

namespace ISAW.DAL
{
    public interface IRepository<T>
    {
        Task<T> GetAsync(Guid Id);
        Task<IEnumerable<T>> GetAsync(IEnumerable<Guid> Ids);
        Task<IEnumerable<T>> GetByFactionAsync(Faction faction);
        Task<IEnumerable<T>> GetByParentIdAsync(Guid parentId);
        Task DeleteAsync(Guid Id);
        Task DeleteAsync(IEnumerable<Guid> Ids);
        Task<Guid> InsertAsync(T entity);
        Task<IEnumerable<Guid>> InsertAsync(IEnumerable<T> entities);
        Task UpdateAsync(T entity);
        Task UpdateAsync(IEnumerable<T> entities);
    }
}
