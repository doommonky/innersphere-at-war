﻿using System;
using Npgsql;
using ISAW.Core;
using System.Threading.Tasks;

namespace ISAW.DAL
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public UnitOfWork(NpgsqlConnection connection)
        {
            Connection = connection;
            Transaction = Connection.BeginTransaction();
        }

        public NpgsqlConnection Connection { get; }
        public NpgsqlTransaction Transaction { get; }

        public bool IsDisposed { get; private set; }


        public async Task CommitAsync()
        {
            await Transaction.CommitAsync().ConfigureAwait(false);
        }

        public void Dispose()
        {
            Transaction?.Dispose();
            Connection.Close();
            IsDisposed = true;
        }

        public async Task RollBackAsync()
        {
            await Transaction.RollbackAsync().ConfigureAwait(false);
        }
    }
}
