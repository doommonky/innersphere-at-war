﻿using Microsoft.Extensions.Configuration;

namespace ISAW.DAL
{
    public class RepositoryBase
    {
        protected readonly IDbContext Context;
        protected string ConnectionString;
        public RepositoryBase(IConfiguration config, IDbContext context)
        {
            Context = context;
            ConnectionString = config.GetConnectionString("PostgreSQL");
        }

        protected string Sanitize(string input)
        {
            if (input == null || input.Equals(string.Empty))
                return null;

            return input.Trim().Replace("'", "''");
        }
    }
}
