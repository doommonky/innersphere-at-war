﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ISAW.Core;
using System.Collections.Generic;
using Npgsql;
using Dapper;
using System.Linq;

namespace ISAW.DAL
{
    public class CombatCommandRepository : RepositoryBase, IRepository<CombatCommand>
    {
        public CombatCommandRepository(IConfiguration config, IDbContext context) : base(config, context) {}

        public async Task DeleteAsync(Guid id)
        {
            await Context.Connection().QueryFirstAsync<CombatCommand>(
                @$"DELETE FROM CombatElement 
                        WHERE Id = '{id}'
                        AND ElementType = '{ElementType.CombatCommand}'", 
                Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task DeleteAsync(IEnumerable<Guid> ids)
        {
            await Context.Connection().QueryAsync<CombatCommand>(
                @$"DELETE FROM CombatElement
                        WHERE Id IN '{string.Join(',', ids.Select(id => id).ToString())}'
                        AND ElementType = '{ElementType.CombatCommand}'",
                Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<CombatCommand> GetAsync(Guid id)
        {
            return await Context.Connection().QueryFirstAsync<CombatCommand>(
                @$"SELECT Id, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, Strength, Location
                        FROM CombatElement
                        WHERE Id = '{id}'
                        AND ElementType = '{ElementType.CombatCommand}'",
                Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<CombatCommand>> GetAsync(IEnumerable<Guid> ids)
        {
            return await Context.Connection().QueryAsync<CombatCommand>(
                @$"SELECT Id, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, Strength, Location
                        FROM CombatElement
                        WHERE Id IN '{string.Join(',', ids.Select(id => id).ToString())}'
                        AND ElementType = '{ElementType.CombatCommand}'", 
                Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<CombatCommand>> GetByFactionAsync(Faction faction)
        {
            return await Context.Connection().QueryAsync<CombatCommand>(
                @$"SELECT Id, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, Strength, Location
                        FROM CombatElement
                        WHERE Allegiance = '{faction.ToString()}'
                        AND ElementType = '{ElementType.CombatCommand}'",
                Context.Transaction())
                .ConfigureAwait(false);
        }

        public Task<IEnumerable<CombatCommand>> GetByParentIdAsync(Guid parentId)
        {
            throw new NotSupportedException();
        }

        public async Task<Guid> InsertAsync(CombatCommand entity)
        {
            var id = Guid.NewGuid();
            await Context.Connection().ExecuteAsync(
                @$"INSERT INTO CombatElement
                    (Id, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, Strength, Location, ElementType)
                    Values ('{id}', '{Sanitize(entity.Name)}', '{entity.Allegiance}', '{entity.ActiveContract}', '{entity.Experience}', 
                    '{entity.Loyalty}', '{entity.Weight}', {entity.Strength}, '{Sanitize(entity.Location)}', '{ElementType.CombatCommand}')",
                Context.Transaction())
                .ConfigureAwait(false);
            return id;
        }

        public async Task<IEnumerable<Guid>> InsertAsync(IEnumerable<CombatCommand> entities)
        {
            var ids = new List<Guid>();
            var valueStrings = new List<string>();
            foreach (var entity in entities)
            {
                var id = Guid.NewGuid();
                ids.Add(id);
                valueStrings.Add($@"('{id}', '{Sanitize(entity.Name)}', '{entity.Allegiance}', '{entity.ActiveContract}', '{entity.Experience}', 
                    '{entity.Loyalty}', '{entity.Weight}', { entity.Strength}, '{Sanitize(entity.Location)}', '{ElementType.CombatCommand}')");
            }

            await Context.Connection().ExecuteAsync(
                @$"INSERT INTO CombatElement
                    (Id, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, Strength, Location, ElementType)
                    Values {string.Join(',', valueStrings)}",
                Context.Transaction())
                .ConfigureAwait(false);
            return ids;
        }

        public async Task UpdateAsync(CombatCommand entity)
        {
            var sql = @$"UPDATE CombatElement
                   SET Name = '{Sanitize(entity.Name)}', Allegiance = '{entity.Allegiance}', ActiveContract = '{entity.ActiveContract}',
                        Experience = '{entity.Experience}', Loyalty = '{entity.Loyalty}', Weight = '{entity.Weight}', Strength = {entity.Strength},
                        Location = '{Sanitize(entity.Location)}', ElementType = '{ElementType.CombatCommand}'
                    WHERE Id = '{entity.Id}'
                    AND ElementType = '{ElementType.CombatCommand}'";

            await Context.Connection().ExecuteAsync(sql, Context.Transaction()).ConfigureAwait(false);
        }

        public async Task UpdateAsync(IEnumerable<CombatCommand> entities)
        {
            foreach (var entity in entities)
            {
                await Context.Connection().ExecuteAsync(
                    @$"UPDATE CombatElement
                   SET Name = '{Sanitize(entity.Name)}', Allegiance = '{entity.Allegiance}', ActiveContract = '{entity.ActiveContract}', 
                        Experience = '{entity.Experience}', Loyalty = '{entity.Loyalty}', Weight = '{entity.Weight}',
                        Strength = {entity.Strength}, Location = '{Sanitize(entity.Location)}', ElementType = '{ElementType.CombatCommand}'
                    WHERE Id = '{entity.Id}'
                    AND ElementType = '{ElementType.CombatCommand}'", Context.Transaction())
                    .ConfigureAwait(false);
            };
        }
    }
}
