﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ISAW.Core;
using System.Collections.Generic;
using Npgsql;
using Dapper;
using System.Linq;

namespace ISAW.DAL
{
    public class RegimentRepository : RepositoryBase, IRepository<Regiment>
    {
        public RegimentRepository(IConfiguration config, IDbContext context) : base(config, context) { }

        public async Task DeleteAsync(Guid id)
        {
            await Context.Connection().QueryFirstAsync<Regiment>(
                @$"DELETE FROM CombatElement 
                        WHERE Id = '{id}'
                        AND ElementType = '{ElementType.Regiment}'")
                .ConfigureAwait(false);
        }

        public async Task DeleteAsync(IEnumerable<Guid> ids)
        {
            await Context.Connection().QueryAsync<Regiment>(
                @$"DELETE FROM CombatElement
                        WHERE Id IN '{string.Join(',', ids.Select(id => id).ToString())}'
                        AND ElementType = '{ElementType.Regiment}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<Regiment> GetAsync(Guid id)
        {
            return await Context.Connection().QueryFirstAsync<Regiment>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType
                        FROM CombatElement
                        WHERE Id = '{id}'
                        AND ElementType = '{ElementType.Regiment}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Regiment>> GetAsync(IEnumerable<Guid> ids)
        {
            var joinedIds = string.Join(',', ids.Select(id => $"'{id}'"));
            var sql = @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type 
                        FROM CombatElement
                        WHERE Id IN ({joinedIds})
                        AND ElementType = '{ElementType.Regiment}'";
            return await Context.Connection().QueryAsync<Regiment>(sql, Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Regiment>> GetByFactionAsync(Faction faction)
        {
            return await Context.Connection().QueryAsync<Regiment>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE Allegiance = '{faction.ToString()}'
                        AND ElementType = '{ElementType.Regiment}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Regiment>> GetByParentIdAsync(Guid parentId)
        {
            return await Context.Connection().QueryAsync<Regiment>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE ParentId = '{parentId}'
                        AND ElementType = '{ElementType.Regiment}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<Guid> InsertAsync(Regiment entity)
        {
            var sql = @$"INSERT INTO CombatElement
                    (Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, ElementType, CompositionType)
                    Values ('{entity.Id}', '{entity.ParentId}', {(entity.Name != null ? "'" + Sanitize(entity.Name) + "'" : "null")}, '{entity.Allegiance}', '{entity.ActiveContract}', '{entity.Experience}', 
                    '{entity.Loyalty}', '{entity.Weight}', '{ElementType.Regiment}', '{entity.Type}')";

            await Context.Connection().ExecuteAsync(sql, Context.Transaction())
                .ConfigureAwait(false);
            return entity.Id;
        }

        public async Task<IEnumerable<Guid>> InsertAsync(IEnumerable<Regiment> entities)
        {
            var valueStrings = new List<string>();
            foreach (var entity in entities)
            {
                valueStrings.Add($@"('{entity.Id}', '{entity.ParentId}', {(entity.Name != null ? "'"+Sanitize(entity.Name)+"'" : "null")}, '{entity.Allegiance}', '{entity.ActiveContract}', '{entity.Experience}', 
                    '{entity.Loyalty}', '{entity.Weight}', '{ElementType.Regiment}', '{entity.Type}')");
            }

            var sql = @$"INSERT INTO CombatElement
                    (Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, ElementType, CompositionType)
                    Values {string.Join(',', valueStrings)}";

            await Context.Connection().ExecuteAsync(sql, Context.Transaction())
                .ConfigureAwait(false);
            return entities.Select(entity => entity.Id);
        }

        public async Task UpdateAsync(Regiment entity)
        {
            var sql = @$"UPDATE CombatElement
                   SET Name = {(entity.Name != null ? "'" + Sanitize(entity.Name) + "'" : "null")}, Allegiance = '{entity.Allegiance}', ActiveContract = '{entity.ActiveContract}',
                        Experience = '{entity.Experience}', Loyalty = '{entity.Loyalty}', Weight = '{entity.Weight}', ElementType = '{ElementType.Regiment}',
                        CompositionType = '{entity.Type}'
                    WHERE Id = '{entity.Id}'
                    AND ElementType = '{ElementType.Regiment}'";

            await Context.Connection().ExecuteAsync(sql, Context.Transaction()).ConfigureAwait(false);
        }

        public async Task UpdateAsync(IEnumerable<Regiment> entities)
        {
            foreach (var entity in entities)
            {
                await Context.Connection().ExecuteAsync(
                    @$"UPDATE CombatElement
                   SET Name = {(entity.Name != null ? "'" + Sanitize(entity.Name) + "'" : "null")}, Allegiance = '{entity.Allegiance}', ActiveContract = '{entity.ActiveContract}', 
                        Experience = '{entity.Experience}', Loyalty = '{entity.Loyalty}', Weight = '{entity.Weight}',
                        ElementType = '{ElementType.Regiment}', CompositionType = '{entity.Type}'
                    WHERE Id = '{entity.Id}'
                    AND ElementType = '{ElementType.Regiment}'", Context.Transaction())
                    .ConfigureAwait(false);
            };
        }
    }
}
