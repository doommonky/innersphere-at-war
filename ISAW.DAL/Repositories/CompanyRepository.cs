﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ISAW.Core;
using System.Collections.Generic;
using Npgsql;
using Dapper;
using System.Linq;

namespace ISAW.DAL
{
    public class CompanyRepository : RepositoryBase, IRepository<Company>
    {
        public CompanyRepository(IConfiguration config, IDbContext context) : base(config, context) { }

        public async Task DeleteAsync(Guid id)
        {
            await Context.Connection().QueryFirstAsync<Company>(
                @$"DELETE FROM CombatElement 
                        WHERE Id = '{id}'
                        AND ElementType = '{ElementType.Company}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task DeleteAsync(IEnumerable<Guid> ids)
        {
            await Context.Connection().QueryAsync<Company>(
                @$"DELETE FROM CombatElement
                        WHERE Id IN '{string.Join(',', ids.Select(id => id))}'
                        AND ElementType = '{ElementType.Company}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<Company> GetAsync(Guid id)
        {
            return await Context.Connection().QueryFirstAsync<Company>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE Id = '{id}'
                        AND ElementType = '{ElementType.Company}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Company>> GetAsync(IEnumerable<Guid> ids)
        {
            return await Context.Connection().QueryAsync<Company>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE Id IN '{string.Join(',', ids.Select(id => id).ToString())}'
                        AND ElementType = '{ElementType.Company}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Company>> GetByFactionAsync(Faction faction)
        {
            return await Context.Connection().QueryAsync<Company>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE Allegiance = '{faction.ToString()}'
                        AND ElementType = '{ElementType.Company}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Company>> GetByParentIdAsync(Guid parentId)
        {
            return await Context.Connection().QueryAsync<Company>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE ParentId = '{parentId}'
                        AND ElementType = '{ElementType.Company}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<Guid> InsertAsync(Company entity)
        {
            await Context.Connection().ExecuteAsync(
                @$"INSERT INTO CombatElement
                    (Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, ElementType, CompositionType)
                    Values ('{entity.Id}', '{entity.ParentId}', {(entity.Name != null ? "'" + Sanitize(entity.Name) + "'" : "null")}, '{entity.Allegiance}', '{entity.ActiveContract}', '{entity.Experience}', 
                    '{entity.Loyalty}', '{entity.Weight}', '{ElementType.Company}', '{entity.Type}')")
                .ConfigureAwait(false);
            return entity.Id;
        }

        public async Task<IEnumerable<Guid>> InsertAsync(IEnumerable<Company> entities)
        {
            var valueStrings = new List<string>();
            foreach (var entity in entities)
            {
                valueStrings.Add($@"('{entity.Id}', '{entity.ParentId}', {(entity.Name != null ? "'" + Sanitize(entity.Name) + "'" : "null")}, '{entity.Allegiance}', '{entity.ActiveContract}', '{entity.Experience}', 
                    '{entity.Loyalty}', '{entity.Weight}', '{ElementType.Company}', '{entity.Type}')");
            }

            await Context.Connection().ExecuteAsync(
                @$"INSERT INTO CombatElement
                    (Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, ElementType, CompositionType)
                    Values {string.Join(',', valueStrings)}", Context.Transaction())
                .ConfigureAwait(false);
            return entities.Select(entity => entity.Id);
        }

        public async Task UpdateAsync(Company entity)
        {
            var sql = @$"UPDATE CombatElement
                   SET Name = '{Sanitize(entity.Name)}', Allegiance = '{entity.Allegiance}', ActiveContract = '{entity.ActiveContract}',
                        Experience = '{entity.Experience}', Loyalty = '{entity.Loyalty}', Weight = '{entity.Weight}', ElementType = '{ElementType.Company}',
                        CompositionType = '{entity.Type}'
                    WHERE Id = '{entity.Id}'
                    AND ElementType = '{ElementType.Company}'";

            await Context.Connection().ExecuteAsync(sql, Context.Transaction()).ConfigureAwait(false);
        }

        public async Task UpdateAsync(IEnumerable<Company> entities)
        {
            foreach (var entity in entities)
            {
                await Context.Connection().ExecuteAsync(
                    @$"UPDATE CombatElement
                   SET Name = '{Sanitize(entity.Name)}', Allegiance = '{entity.Allegiance}', ActiveContract = '{entity.ActiveContract}', 
                        Experience = '{entity.Experience}', Loyalty = '{entity.Loyalty}', Weight = '{entity.Weight}',
                        ElementType = '{ElementType.Company}', CompositionType = '{entity.Type}'
                    WHERE Id = '{entity.Id}'
                    AND ElementType = '{ElementType.Company}'", Context.Transaction())
                    .ConfigureAwait(false);
            };
        }
    }
}
