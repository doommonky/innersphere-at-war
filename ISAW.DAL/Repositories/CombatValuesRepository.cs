﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ISAW.Core;
using System.Collections.Generic;
using Npgsql;
using Dapper;
using System.Linq;

namespace ISAW.DAL
{
    public class CombatValuesRepository : RepositoryBase, IRepository<CombatValues>
    {
        public CombatValuesRepository(IConfiguration config, IDbContext context) : base(config, context) {}

        public async Task DeleteAsync(Guid id)
        {
            await Context.Connection().QueryFirstAsync<CombatValues>(
                @$"DELETE FROM CombatValue
                        WHERE Id = '{id}'", 
                Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task DeleteAsync(IEnumerable<Guid> ids)
        {
            await Context.Connection().QueryAsync<CombatValues>(
                @$"DELETE FROM CombatValue
                        WHERE Id IN '{string.Join(',', ids.Select(id => id).ToString())}'",
                Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<CombatValues> GetAsync(Guid id)
        {
            return await Context.Connection().QueryFirstAsync<CombatValues>(
                @$"SELECT Id, ParentId, Move, TransportMove, Jump, TMM, ARM, Short, Medium, Long, EAero, PointsValue, SpecialRules
                        FROM CombatValue
                        WHERE Id = '{id}'", 
                Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<CombatValues>> GetAsync(IEnumerable<Guid> ids)
        {
            return await Context.Connection().QueryAsync<CombatValues>(
                @$"SELECT Id, ParentId, Move, TransportMove, Jump, TMM, ARM, Short, Medium, Long, EAero, PointsValue, SpecialRules
                        FROM CombatValue
                        WHERE Id IN ({string.Join(',', ids.Select(id => $"'{id}'").ToString())})",
                Context.Transaction())
                .ConfigureAwait(false);
        }

        public Task<IEnumerable<CombatValues>> GetByFactionAsync(Faction faction)
        {
            throw new NotSupportedException();
        }

        public async Task<IEnumerable<CombatValues>> GetByParentIdAsync(Guid parentId)
        {
            return await Context.Connection().QueryAsync<CombatValues>(
                @$"SELECT Id, ParentId, Move, TransportMove, Jump, TMM, ARM, Short, Medium, Long, EAero, PointsValue, SpecialRules
                        FROM CombatValue
                        WHERE parentId = '{parentId}'"
                        , Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<Guid> InsertAsync(CombatValues entity)
        {
            var id = Guid.NewGuid();
            var sql = @$"INSERT INTO CombatValue
                    (Id, ParentId, Move, TransportMove, Jump, TMM, ARM, Short, Medium, Long, EAero, PointsValue, SpecialRules)
                    Values ('{id}', '{entity.ParentId}', {entity.Move}, {(entity.TransportMove.HasValue ? entity.TransportMove.ToString() : "null")}, 
                    {(entity.Jump.HasValue ? entity.Jump.ToString() : "null")}, {entity.TMM}, {entity.ARM}, {entity.Short}, {entity.Medium}, {entity.Long}, 
                    {(entity.EAero.HasValue ? entity.EAero.ToString() :"null")}, {entity.PointsValue}, '{{{string.Join(",", entity.SpecialRules.Select(s => $"\"{s}\""))}}}')";

            await Context.Connection().ExecuteAsync(sql, Context.Transaction()).ConfigureAwait(false);
            return id;
        }

        public async Task<IEnumerable<Guid>> InsertAsync(IEnumerable<CombatValues> entities)
        {
            var ids = new List<Guid>();
            var valueStrings = new List<string>();
            foreach (var entity in entities)
            {
                var id = Guid.NewGuid();
                ids.Add(id);
                valueStrings.Add($@"('{id}', '{entity.ParentId}', {entity.Move}, '{(entity.TransportMove.HasValue ? entity.TransportMove.ToString() : "null")}',
                    '{(entity.Jump.HasValue ? entity.Jump.ToString() : "null")}', {entity.TMM}, {entity.ARM}, {entity.Short}, {entity.Medium}, {entity.Long}, 
                    '{(entity.EAero.HasValue ? entity.EAero.ToString() : "null")}', {entity.PointsValue}, '{{{string.Join(",", entity.SpecialRules.Select(s => $"\"{s}\""))}}}')");
            }

            await Context.Connection().ExecuteAsync(
                @$"INSERT INTO CombatValue
                    (Id, ParentId, Move, TransportMove, Jump, TMM, ARM, Short, Medium, Long, EAero, PointsValue, SpecialRules)
                    Values {string.Join(',', valueStrings)}", Context.Transaction())
                .ConfigureAwait(false);
            return ids;
        }

        public async Task UpdateAsync(CombatValues entity)
        {
            var sql = @$"UPDATE CombatValue
                   SET Move = '{entity.Move}', TransportMove = '{(entity.TransportMove.HasValue ? entity.TransportMove.ToString() : "null")}',
                    Jump = '{(entity.Jump.HasValue ? entity.Jump.ToString() : "null")}', TMM = '{entity.TMM}', ARM = '{entity.ARM}', Short = '{entity.Short}', Medium = {entity.Medium},
                        Long = '{entity.Long}', EAero = '{(entity.EAero.HasValue ? entity.EAero.ToString() : "null")}', PointsValue = '{entity.PointsValue}', 
                        SpecialRules = {{{string.Join(",", entity.SpecialRules)}}}
                    WHERE Id = '{entity.Id}'";

            await Context.Connection().ExecuteAsync(sql, Context.Transaction()).ConfigureAwait(false);
        }

        public async Task UpdateAsync(IEnumerable<CombatValues> entities)
        {
            foreach (var entity in entities)
            {
                await Context.Connection().ExecuteAsync(
                    @$"UPDATE CombatValue
                   SET Move = '{entity.Move}', TransportMove = '{(entity.TransportMove.HasValue ? entity.TransportMove.ToString() : "null")}',
                        Jump = '{(entity.Jump.HasValue ? entity.Jump.ToString() : "null")}', TMM = '{entity.TMM}', ARM = '{entity.ARM}', Short = '{entity.Short}', Medium = {entity.Medium},
                        Long = '{entity.Long}', EAero = '{(entity.EAero.HasValue ? entity.EAero.ToString() : "null")}', PointsValue = '{entity.PointsValue}', 
                        SpecialRules = {{{string.Join(",", entity.SpecialRules)}}}
                    WHERE Id = '{entity.Id}'", Context.Transaction())
                    .ConfigureAwait(false);
            };
        }
    }
}
