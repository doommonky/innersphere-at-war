﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ISAW.Core;
using System.Collections.Generic;
using Npgsql;
using Dapper;
using System.Linq;

namespace ISAW.DAL
{
    public class LanceRepository : RepositoryBase, IRepository<Lance>
    {
        public LanceRepository(IConfiguration config, IDbContext context) : base(config, context) { }

        public async Task DeleteAsync(Guid id)
        {
            await Context.Connection().QueryFirstAsync<Lance>(
                @$"DELETE FROM CombatElement 
                        WHERE Id = '{id}'
                        AND ElementType = '{ElementType.Lance}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task DeleteAsync(IEnumerable<Guid> ids)
        {
            await Context.Connection().QueryAsync<Lance>(
                @$"DELETE FROM CombatElement
                        WHERE Id IN '{string.Join(',', ids.Select(id => id).ToString())}'
                        AND ElementType = '{ElementType.Lance}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<Lance> GetAsync(Guid id)
        {
            return await Context.Connection().QueryFirstAsync<Lance>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE Id = '{id}'
                        AND ElementType = '{ElementType.Lance}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Lance>> GetAsync(IEnumerable<Guid> ids)
        {
            return await Context.Connection().QueryAsync<Lance>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE Id IN '{string.Join(',', ids.Select(id => id).ToString())}'
                        AND ElementType = '{ElementType.Lance}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Lance>> GetByFactionAsync(Faction faction)
        {
            return await Context.Connection().QueryAsync<Lance>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE Allegiance = '{faction.ToString()}'
                        AND ElementType = '{ElementType.Lance}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Lance>> GetByParentIdAsync(Guid parentId)
        {
            return await Context.Connection().QueryAsync<Lance>(
                @$"SELECT Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, CompositionType as Type
                        FROM CombatElement
                        WHERE ParentId = '{parentId}'
                        AND ElementType = '{ElementType.Lance}'", Context.Transaction())
                .ConfigureAwait(false);
        }

        public async Task<Guid> InsertAsync(Lance entity)
        {
            var sql = @$"INSERT INTO CombatElement
                    (Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, ElementType, CompositionType)
                    Values ('{entity.Id}', '{entity.ParentId}', {(entity.Name != null ? "'" + Sanitize(entity.Name) + "'" : "null")}, '{entity.Allegiance}', '{entity.ActiveContract}', '{entity.Experience}', 
                    '{entity.Loyalty}', '{entity.Weight}', '{ElementType.Lance}', '{entity.Type}')";
            await Context.Connection().ExecuteAsync(sql, Context.Transaction())
                .ConfigureAwait(false);
            return entity.Id;
        }

        public async Task<IEnumerable<Guid>> InsertAsync(IEnumerable<Lance> entities)
        {
            var valueStrings = new List<string>();
            foreach (var entity in entities)
            {
                valueStrings.Add($@"('{entity.Id}', '{entity.ParentId}', {(entity.Name != null ? "'" + Sanitize(entity.Name) + "'" : "null")}, '{entity.Allegiance}', '{entity.ActiveContract}', '{entity.Experience}', 
                    '{entity.Loyalty}', '{entity.Weight}', '{ElementType.Lance}', '{entity.Type}')");
            }

            await Context.Connection().ExecuteAsync(
                @$"INSERT INTO CombatElement
                    (Id, ParentId, Name, Allegiance, ActiveContract, Experience, Loyalty, Weight, ElementType, CompositionType)
                    Values {string.Join(',', valueStrings)}", Context.Transaction())
                .ConfigureAwait(false);
            return entities.Select(entity => entity.Id);
        }

        public async Task UpdateAsync(Lance entity)
        {
            var sql = @$"UPDATE CombatElement
                   SET Name = '{Sanitize(entity.Name)}', Allegiance = '{entity.Allegiance}', ActiveContract = '{entity.ActiveContract}',
                        Experience = '{entity.Experience}', Loyalty = '{entity.Loyalty}', Weight = '{entity.Weight}', ElementType = '{ElementType.Lance}',
                        CompositionType = '{entity.Type}'
                    WHERE Id = '{entity.Id}'
                    AND ElementType = '{ElementType.Lance}'";

            await Context.Connection().ExecuteAsync(sql, Context.Transaction()).ConfigureAwait(false);
        }

        public async Task UpdateAsync(IEnumerable<Lance> entities)
        {
            foreach (var entity in entities)
            {
                await Context.Connection().ExecuteAsync(
                    @$"UPDATE CombatElement
                   SET Name = '{Sanitize(entity.Name)}', Allegiance = '{entity.Allegiance}', ActiveContract = '{entity.ActiveContract}', 
                        Experience = '{entity.Experience}', Loyalty = '{entity.Loyalty}', Weight = '{entity.Weight}',
                        ElementType = '{ElementType.Lance}', CompositionType = '{entity.Type}'
                    WHERE Id = '{entity.Id}'
                    AND ElementType = '{ElementType.Lance}'", Context.Transaction())
                    .ConfigureAwait(false);
            };
        }
    }
}
