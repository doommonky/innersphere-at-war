﻿using ISAW.Core;
using System.Threading.Tasks;

namespace ISAW.DAL
{
    public interface IUnitOfWork
    {
        Task RollBackAsync();
        Task CommitAsync();
        bool IsDisposed { get; }
    }
}
