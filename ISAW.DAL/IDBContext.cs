﻿using Npgsql;

namespace ISAW.DAL
{
    public interface IDbContext
    {
        NpgsqlConnection Connection();
        NpgsqlTransaction Transaction();
        UnitOfWork Create();

    }
}
