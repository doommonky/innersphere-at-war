﻿using System;
using System.Threading.Tasks;
using ISAW.Core;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace ISAW.DAL
{
    public class DbContext : IDbContext
    {
        private readonly IConfiguration _config;
        private NpgsqlConnection _connection;
        private UnitOfWork _unitOfWork;

        public DbContext(IConfiguration config)
        {
            _config = config;
        }

        private bool UnitOfWorkIsActive => !(_unitOfWork == null || _unitOfWork.IsDisposed);

        public NpgsqlConnection Connection()
        {
            if (!UnitOfWorkIsActive)
            {
                throw new InvalidOperationException("There is no unit of work to create a connection.");
            }

            return _unitOfWork.Connection;
        }

        public NpgsqlTransaction Transaction()
        {
            if (!UnitOfWorkIsActive)
            {
                throw new InvalidOperationException("There is no unit of work to pull transaction.");
            }

            return _unitOfWork.Transaction;
        }

        public UnitOfWork Create()
        {
            if (UnitOfWorkIsActive)
            {
                throw new InvalidOperationException("Cannon begin a new transaction before the previous is disposed.");
            }

            _connection = new NpgsqlConnection(_config.GetConnectionString("PostgreSQL"));
            _connection.Open();
            _unitOfWork = new UnitOfWork(_connection);

            return _unitOfWork;
        }
    }
}
