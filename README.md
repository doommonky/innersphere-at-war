# Battletech: Innersphere at War!
## A Battletech companion app

This application is designed as a campaign generation and management tool to run the _Innersphere at War_ campaign ruleset.

### Required tools for development:
You will need to install the following programs:

* Microsoft Visual Studio 2019 or higher - https://visualstudio.microsoft.com/vs/
* .NET Core 3.1 SDK or higher - https://dotnet.microsoft.com/download/dotnet/
* Node v12.2.0 or higher - https://nodejs.org/en/download/
* PostgreSQL 13.2 or higher - https://www.postgresql.org/download/
* GIT - https://git-scm.com/downloads

Recommended tools

* A SQL query app like Beekeeper.io
* Another lightweight text editor like VS code
* A GIT GUI application like Sourcetree if desired

### To run:
* Install PostgreSQL
* Create a database named "ISAW"
* Restore ISAW_SchemaOnly from ISAW.DAL

* Open the .NET solution in Visual Studio
* Select Build => Build Solution from the navigation bar
* Select ISAW.Web as the startup app in the dropdown at the top or by right clicking ISAW.Web in the solution explorer and selecting "Set as Startup App"
* Click the "IIS Express" run button at the top

* In a command or powershell window, navigate to the ISAW.UI folder
* Type "npm install"
* Type "npm run start"

At this point, the entire application should be running and each part should be connected to each other.